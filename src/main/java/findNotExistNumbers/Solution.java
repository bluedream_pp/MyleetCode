package findNotExistNumbers;

import java.util.BitSet;

/*
* 假设有一个数组，里面是正整数，找出里面缺少的最小的数字。例如，1、2、4、5、7，他里面缺少 3 和 6，把 3 找出来即可。
* */
public class Solution {
    public static int solution(int[] arr){
        int rs = 0 ;
        BitSet set = new BitSet();
        for (int i : arr) {
            set.set(i);
        }
        int l = set.length();
        for (int i = 1; i < l; i++) {
            if(!set.get(i)){
                System.out.println(i);
                rs = i ;
                break;
            }
        }
        return rs ;
    }
    public static void main(String[] args) {
//        int[] array = new int[]{1,2,4,5,7};
        int[] array1 = new int[]{1,2,3,4,5,6,9};
        solution(array1);
    }
}
