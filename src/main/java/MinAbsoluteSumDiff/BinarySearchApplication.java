package MinAbsoluteSumDiff;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @className: BinarySearchApplication
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/10/30 20:51
 */
public class BinarySearchApplication {
    private static Logger logger = LoggerFactory.getLogger(BinarySearchApplication.class);
    /**
     * array 是序的 查找里 targer 值最进的那个数字
     * */
    public static int binarySearch(int[] array , int target){
        int L = 0 ;
        int H = array.length -1 ;
        int mid = (L + H) >> 1 ;
        int idx = 0 ;
        boolean low = false ;
        while(L < H){
            if(array[mid] == target){
                idx = mid;
                break ;
            }else if(array[mid] < target){
                // 再往右边找，可能找到第一个比它大的，也可能找不到
                L = mid + 1 ;
            }else if(array[mid] > target){
                H = mid ;
            }
            mid = (L + H) >> 1 ;
        }
        return L;
    }
    public static void main(String[] args){
        int[] array = { 1 , 2 , 3 , 7 , 110 , 120} ;
        int a = binarySearch(array , 113);
        System.out.println(a);
    }
}
