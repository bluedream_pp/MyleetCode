package CLH;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) throws InterruptedException {
//        CLHLock clh = new CLHLock();
//        Thread thread = new Thread(() -> {
//            clh.lock();
//        },"thread");
//        thread.start();
//        TimeUnit.SECONDS.sleep(1);
//
//        Thread thread1 = new Thread(() -> {
//            clh.lock();
//        },"thread1");
//        thread1.start();
        ReentrantLock lock = new ReentrantLock();
        Thread t1 = new Thread(()->{
            lock.lock();
            try{
                System.out.println(Thread.currentThread().getName() + "do my thing");
            }finally {
//                lock.unlock();
            }
        }, "t1");

        Thread t2 = new Thread(()-> {
//            lock.lock();
//            try {
//                System.out.println(Thread.currentThread().getName() + "do my thing");
//            } finally {
//                lock.unlock();
//            }
            while (true){
                try {
                    TimeUnit.SECONDS.sleep(1);
                    System.out.println("t2 run");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
         , "t2");


//        t1.start();
        TimeUnit.SECONDS.sleep(2);
        t2.start();
        LockSupport.park(t2);
        int n = 0 ;
        while(n<20){
            TimeUnit.SECONDS.sleep(1);
            System.out.println("t2 thread state: " + t2.getState());
            n++;
        }
        LockSupport.unpark(t2);
        System.out.println("t2 thread state: " + t2.getState());
    }
}
