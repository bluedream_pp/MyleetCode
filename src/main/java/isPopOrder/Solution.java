package isPopOrder;

public class Solution {
    private int[] data = new int[100];
    private int idx = 0 ;

    public void push(int i){
        data[idx++] = i ;
    }
    public Integer pop(){
        Integer rs = null ;
        if( --idx >= 0 ){
            rs = data[idx];
        }
        return rs ;
    }
    public Integer top(){
        return data[idx==0?0:idx-1];
    }
    public int size(){
        return idx  ;
    }
    public boolean isEmpute(){
        if(size() == 0 ){
            return true;
        }else{
            return false ;
        }
    }
    public boolean isPopOrder(int[] pushOrder , int[] popOrder ){
        int pushIdx = 0 ;
        int popIdx = 0 ;
        while( pushIdx < pushOrder.length){
            if(pushOrder[pushIdx] != popOrder[popIdx] ){
                push(pushOrder[pushIdx++]);
            }else {
                pushIdx++;
                popIdx++;
                while( popIdx < popOrder.length && top() == popOrder[popIdx]){
                    pop();
                    popIdx++;
                }
            }

        }
       return !isEmpute();
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        int[] pushOrder = {1,2,3,4,5};
        int[] popOrder = {4,3,5,2,1};
        System.out.println(s.isPopOrder(pushOrder,popOrder));
    }
}
