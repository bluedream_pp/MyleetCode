package replaceBlank;

/*
*
* 请实现一个函数，将一个字符串中的每个空格替换成“%20”。例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
* */

import java.util.Arrays;

public class Solution {
    public void solution( String str ){
        if(null == str || "".equals(str)){
            System.out.println("invalidate input");;
            return;
        }
        char[] chars = str.toCharArray();
        char[] newChars = resize(chars,100);
        int oldLength = chars.length;
        int blankLength = 0 ;
        for(char c : chars){
            if(' ' == c){
                blankLength++;
            }
        }
        int newLength = oldLength + blankLength*2;

        for(int i = oldLength -1 ; i >=0 ; i--){
           if(' ' == newChars[i]){

               newChars[newLength-1] = '%';
               newLength--;
               newChars[newLength-1] = '2';
               newLength--;
               newChars[newLength-1] = '0';
               newLength--;
           }else{
               newChars[newLength-1] = newChars[i];
               newLength--;
           }
        }

        System.out.println(newChars);

    }
    private char[] resize( char[] a , int newLength){
        char[] b = Arrays.copyOf(a , newLength);
       return b ;
    }
}
