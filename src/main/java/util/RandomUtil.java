package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @className: RandomUtil
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/13 22:16
 */
public class RandomUtil {
    public static int randInt(int n){
        return (int) (Math.random() * Math.pow(10 , n));
    }
    public static boolean randBoolean(){
        return Math.random() > 0.5D ;
    }
    public static int randIntRange(int carry , int from, int to){
        int i = randInt(carry);
        while(i < from || i > to){
            i = randInt(carry);
        }
        return i ;
    }
    public static char randChar(){
        int i = randInt(1);
        while(i > 26){
            i = randInt(1);
        }
        return (char)(i + 'a');
    }
    public static String randString(int n){
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < n ; i++){
            sb.append(randChar());
        }
        return sb.toString();
    }
}
