package StringLearn.strToInt;

public class Solution {

    private Integer strToInt(String str){
        char[] chars = str.toCharArray();
        Integer rs = 0 ;
        boolean positive = true;
        if(chars.length==0){
            return 0;
        }
        for(int i = 0 ; i < chars.length; i++){
            if('0' <= chars[i] && chars[i] <= '9'){
                rs = rs*10 + chars[i] - '0';
            }else{
                if('-' == chars[i] ){
                    positive = false;
                }else{
                    return 0 ;
                }
            }
        }
        return positive?rs:-1*rs;
    }

    public static void main(String[] args) {
        System.out.println(new Solution().strToInt("123"));
        System.out.println(new Solution().strToInt("123a"));
        System.out.println(new Solution().strToInt("1239888"));
        System.out.println(new Solution().strToInt("-1239888"));
    }
}
