package StringLearn;

public class NewString {
    public static void main(String[] args) {
        String str = "abc";
        String str2 = new String(str);
        char[] chars = {'a','b','c'};
        String str3 = new String(chars);
        System.out.println(str2 == str);
        System.out.println(str == str3);
    }
}
