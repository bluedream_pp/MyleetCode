package leftRotateString;

import java.util.Arrays;

public class Solution {
    public String leftRotateString(String S, int n){
        char[] chs = S.toCharArray();
        char[] chars = Arrays.copyOf(chs, chs.length + n);
        for(int i = 0 ; i < n ; i++){
            swap(chars , i , chs.length+i);
        }

        return String.valueOf(Arrays.copyOfRange(chars,n,chars.length));
    }
    public String leftRotateString2(String in , int n){
        char[] chs = in.toCharArray();
        StringBuilder rs = new StringBuilder();
        for(int i = 0 ; i< n || i+n < chs.length;i++){
            if(i < n ){
                rs.append(chs[i]);
            }
            rs.insert(i,chs[i+n]);
        }
        return rs.toString();
    }
    public void swap(char[] array , int start , int end){
        char temp = array[start];
        array[start] = array[end];
        array[end] = temp ;
    }
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.leftRotateString2("abcddSAASASAS" ,5));
    }
}
