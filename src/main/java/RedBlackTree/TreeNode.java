package RedBlackTree;

public class TreeNode {
    private String color ;
    private Object k ;
    private Object v ;
    private TreeNode left ;
    private TreeNode right;

    public TreeNode(String color, Object k, Object v) {
        this.color = color;
        this.k = k;
        this.v = v;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Object getK() {
        return k;
    }

    public void setK(Object k) {
        this.k = k;
    }

    public Object getV() {
        return v;
    }

    public void setV(Object v) {
        this.v = v;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }
}
