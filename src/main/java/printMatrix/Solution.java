package printMatrix;
/**
 * Solution class decibtion
* 输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字，例如，如果输入如下4 X 4矩阵： 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 则依次打印出数字1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10.
*
* 1. 解题思路是这样的，使用 direction 变量作为方向，如下所示， direction = 1 作为初始值
* // 0 1 -> 0 左 ： 1 右
* // 2 3 -> 2 上 ： 3 下
* 2. vertical 作为行号，horizon 作为列号。初始值都为 0
* 3. 当 direction = 1 的时候检测是否碰壁，如果没有碰壁，则让 vertical ++ ; 否则向下走。
* 4. 当 direction = 3 的时候检测是否碰壁，如果没有碰壁，则让 horizon ++ ； 否则向左
* 5. 当 direction = 0 的时候检测是否碰壁，如果没有碰壁，则让 horizon -- ； 否则向上
* 6. 当 direction = 2 的时候检测是否碰壁，如果没有碰壁，则让 vertical -- ； 否则向右
 * 7.走过的地方，使用 -1 来标识。
* */
public class Solution {
    public void forMatix( int[][] matrix){
        for( int i = 0 ; i < matrix.length ; i++){
            for(int j = 0 ; j < matrix[0].length ; j++){
                System.out.println(matrix[i][j]);
            }
        }
    }

    public void printMatrix(int[][] matrix){
        int length = matrix.length*matrix[0].length;
        int l = matrix[0].length;
        int h = matrix.length;
        int li = 0 ;
        int hi = 0 ;
        int vertical = 0 ;
        int horizon = 0 ;
        // 0 1 -> 0 左 ： 1 右
        // 2 3 -> 2 上 ： 3 下
        int direction = 1;
        for( int i = 0 ; i < length ; i++){
            System.out.println(matrix[vertical][horizon]);
            matrix[vertical][horizon] = -1 ;
            if( 0 == direction){
                if( horizon == 0 || ( horizon >= 0 && matrix[vertical][horizon] == -1 )){
                    vertical--;
                    direction = 2;
                }else{
                    horizon--;
                }
            } else if( 1 == direction){
                if(horizon+1 == l || ( horizon + 1 <= l  && matrix[vertical][horizon+1] == -1 )){
                    vertical++;
                    direction = 3;
                }else{
                    horizon++;
                }
            } else if( 2 == direction){
                if(horizon == 0 || ( horizon <= 0 && matrix[vertical][horizon] == -1)){
                    vertical++;
                    direction = 1;
                }else{
                    horizon--;
                }
            } else if( 3 == direction){
                // 往左转
                if( vertical + 1 == h || ( vertical+1  <= h && matrix[vertical+1][horizon] == -1)){
                    horizon--;
                    direction = 0 ;
                }else{
                    vertical++;
                }
            }
        }

    }

    public static void main(String[] args) {
        int[][] matrix = {
                {1,2,3,4,5},
                {6,7,8,7,8},
                {9,10,11,12,13},
                {14,15,16,17,18}
        };
        new Solution().printMatrix(matrix);
    }
}
