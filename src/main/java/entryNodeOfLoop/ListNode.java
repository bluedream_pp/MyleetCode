package entryNodeOfLoop;

/**
 * @author wangyifei
 */
public class ListNode {
    private int var ;
    private ListNode next ;

    public ListNode(int var) {
        this.var = var;
    }
    public int getVar() {
        return var;
    }

    public void setVar(int var) {
        this.var = var;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }
}
