package interview.StringSome;

import java.lang.reflect.Field;

public class ModifyString {
    public static void main(String[] args) {
        String nihao = "shenqing" ;
        /**
         * String 的字符串，其实是保存在 char[] 数组的，所以我们使用
         * 反射机制改变它的值就可以了。
         *
         * */
        try {
            Class<? extends String> klass = nihao.getClass();
            Field value = klass.getDeclaredField("value");
            value.setAccessible(Boolean.TRUE);
            /**
             * getClass 运行时候名字，getDeclaringClass 返回被定义的类名称。
             * 这就设置到多态的性值. 集中体现在父类和接口上面。
             * Humen hu = new Worker() ;
             * Humen hu2 = new Teacher() ;
             * Humen 类可以指向 Worker 和 Teacher 。这就是一种多态。
             * 如果的执行 hu 对应的 getClass，它的返回的是 Humen ， 而 getDeclaringClass 返回的是
             * Worker。
             * Ungeilivaible(不给力的) un = new Dog()
             * Ungeilivaible(不给力的) un1 = new Cat()
             * 不给的可以能是 Dog ，也可以是 Cat
             */
            //
            System.out.println("修改前的值");
            System.out.println((char[]) value.get(nihao));
            // 修改它的值
            value.set(nihao,"wangyifei".toCharArray());
            System.out.println("修改后的值");
            System.out.println((char[]) value.get(nihao));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
