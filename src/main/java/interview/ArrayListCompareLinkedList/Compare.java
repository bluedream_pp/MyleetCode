package interview.ArrayListCompareLinkedList;

import java.util.ArrayList;
import java.util.LinkedList;

public class Compare {
    public static void main(String[] args) {
        /**
         * 不同点：
         * 1. linkedList 实现了 Deque 接口，所以它有双向的队列性值。
         * 2. 二者的数据结构不同。前者是 array ，后者是双向链表。
         * 3. 插入的耗时不同，前者插入的时间复杂度可能是 O(1) ， 也可能是O(n)
         *    如果在 array 的最后位置插入，那时间复杂度就是 O(1)
         *    如果 插入的位置在中间，就是 O(n-k)
         *    LinkedList 插入的时间复杂度都是 O(n/2)
         * 4. 前者需要扩容，后者不会有扩容
         * */
        ArrayList<Integer> array = new ArrayList<>();
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        array.add(3,6);
        LinkedList<Integer> linked = new LinkedList<>();
        linked.add(1);
        linked.add(1);
        linked.add(1);
        linked.add(1);
        linked.add(3,2);
    }
}
