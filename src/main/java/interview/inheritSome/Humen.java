package interview.inheritSome;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Humen {
    private double money =1;
    /**
     * 只有 public 类型的权限类型，子类才能访问变量，才能使用反射来取到相关的变量。
     * */
    public String name ;
     int age ;
    protected String sex ;

    public static void main(String[] args) {
        /**
         *  多态性的测试
         *  这里又复习一下内部类的使用
         * */
        Humen hu = new Humen().new Workder();
        Humen hu2 = new Humen.Teacher();

        System.out.println("hu 的引用类型：" + hu.getClass().getDeclaringClass());
        System.out.println("hu 的指向类型：" + hu.getClass().getName());

        /**
         * 在来一个知识点：子类是可以继承父类的私有方法的，只是看不到而已。可以使用反射的方式来证明。
         * */
        Teacher t = new Teacher();
        try {
            // 使用反射访问不了 age 变量， age 变量是默认类型
            Field age = t.getClass().getDeclaredField("age");
            age.setAccessible(Boolean.TRUE);
            System.out.println(age.get(t));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    class Workder extends Humen{
        String title ;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
    static class Teacher extends Humen{
        private String title ;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
