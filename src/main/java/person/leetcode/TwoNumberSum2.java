package person.leetcode;

import java.util.BitSet;

public class TwoNumberSum2 {
	public static void main(String[] args){
	    Integer[] someNumbers = new Integer[]{1,3,4,6,8};	
	    int sum = 11 ;
	    BitSet exists = new BitSet();
	    for(int i = 0 ; i < someNumbers.length ; i++){
	    	if(!exists.get(sum - someNumbers[i])){
	    		exists.set(someNumbers[i]);
	    	}else{
	    		System.out.println( (sum -  someNumbers[i])+ " and "+  someNumbers[i] + " = " + sum);
	    	}
	    }
	}
}
