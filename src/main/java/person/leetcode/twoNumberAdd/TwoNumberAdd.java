package person.leetcode.twoNumberAdd;
/*
 * 使用链表记录两个数非负整数，实现这两个数的相加，同理相乘的操作也能实现
 * */
public class TwoNumberAdd {
   public static void main(String[] args){
	test("123","123") ;
	test("0","12133455") ;
	test("1231","0") ;
	test("0","0") ;
	test("1231244334","12133455") ;
	test("433423231223","12133455") ;
	test("9","9") ;
	test("999","1") ;
	test("9999","888") ;
	
    // above is '+' and we can do more about '	-','*','/' and so on . And wo can handle with binay number and arbitrary carry( 进位的意思)
	
   }
  
   public static void test(String num1 , String num2 ){
	 System.out.println(num1 + " + " + num2  + " is :" );
	 TwoNumberAdd instance = new TwoNumberAdd();
	  NumberNode result = instance.addNumbers(instance.packNumber(num1) ,  instance.packNumber(num2));
	  instance.printNode(result); 
   }
/*
 *desc: add the two numbers 
 *return: the first node of the sum
 * */   
  public NumberNode addNumbers(NumberNode num1 , NumberNode num2) {
      Integer carried = 0 ;
      boolean run = true ;
      NumberNode result  = null ;
      NumberNode preNode = null ;
      NumberNode currNode1 = num1 ;
      NumberNode currNode2 = num2;
      Integer sum = 0 ;
      while(run){
    	 if(null == currNode1 && null == currNode2) {
    		run = false; 
    		break;
    	 }
        result = new NumberNode() ; 
        
        sum = (currNode1 == null ? 0 : currNode1.getNodeValue())
        		   + ( currNode2 == null ? 0 : currNode2.getNodeValue()) 
        		   + carried;

    	result.setNodeValue(sum%10  ); 
    	carried = sum/10;
    	currNode1 = null == currNode1 ? null:currNode1.getNext(); 
    	currNode2 = null == currNode2 ? null:currNode2.getNext(); 
    	result.setNext(preNode);
    	preNode    = result; 
      }
      
      if(carried > 0) {
    	 preNode = result ; 
    	 result = new NumberNode();
    	 result.setNodeValue(carried);
    	 result.setNext(preNode);
      }
      
	  return result;
  }
   
   
   
   /*
    *desc:将输入的两个字符类型的数字打包成 NumberNode 的格式，
    *return： 这个数字的开头的环节
    * */
   public NumberNode packNumber(String number1){
      NumberNode header = null ;
      NumberNode preNode = null ;
      NumberNode currentNode = null ;
      for(char c : number1.toCharArray()){
          currentNode = new NumberNode();
    	  currentNode.setNodeValue(Integer.parseInt(String.valueOf(c)));
    	  if( null != preNode ){
    		 currentNode.setNext(preNode); 
    	  }
    	  preNode = currentNode ;
    	  header    = currentNode ;
      }
	  return header ; 
   }
   /*
    * desc:print the number
    * */
   public void printNode(NumberNode header){
	   boolean run = true;
	   NumberNode currentNode = header;
       StringBuilder sb  = new StringBuilder();
	    while(run) {
	        if(null == currentNode.getNext()){
	        	run = false;
	        }
	        if( null != currentNode ){
	        	sb.append(currentNode.getNodeValue());
	        }
	        currentNode = currentNode.getNext();  
	    }
	   System.out.println("the sum is :" + sb.toString());
	   System.out.println();

//	   char[] chars = sb.toString().toCharArray();
//       for(int i =chars.length-1 ;  i >= 0 ;i-- ) {
//    	   System.out.print(chars[i]);
//       }
   }
}
