package person.leetcode;

import java.util.HashMap;
import java.util.Map;

public class TwoNumberSum {
	public static void main(String[] args){
	    Integer[] someNumbers = new Integer[]{1,3,4,6,8};	
	    int sum = 11 ;
        /*
         * 此算法的核心是引入了，HashMap 这种结构来保存出现过的数字，有了他就不用两次循环了
         * 其实还可以使用  bitSet 来加快查询的速度
         * */
	    Map<Integer,Integer> exists = new HashMap<Integer,Integer>();
	    for(int i =0 ;  i < someNumbers.length ; i++){
	    	if(!exists.containsKey(sum - someNumbers[i])){
	    		exists.put(someNumbers[i],someNumbers[i]);
	    	}else{
	    		System.out.println( (sum -  someNumbers[i])+ " and "+  someNumbers[i] + " = " + sum);
	    	}
	    }
	}
}
