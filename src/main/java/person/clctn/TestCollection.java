package person.clctn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Spliterator;

public class TestCollection {
    public static void main(String[] args) {
        ArrayList<String> ls = new ArrayList<>();
        ls.add("James");
        ls.add("Kobe");
        ls.add("Ivenson");
        ls.add("yaoming");
        ls.add("Obama");
        ls.add("Biden");
        Spliterator<String> spliterator = ls.spliterator();
        Spliterator<String> spliterator2 =  spliterator.trySplit();
        Spliterator<String> spliterator3 =  spliterator.trySplit();

        spliterator.forEachRemaining( a -> System.out.println(a));

//        System.out.println("----------");
//        spliterator2.forEachRemaining( a -> System.out.println(a));
//
//        System.out.println("----------");
//        spliterator3.forEachRemaining( a -> System.out.println(a));
    }
}
