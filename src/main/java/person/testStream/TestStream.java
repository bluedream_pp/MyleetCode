package person.testStream;

import java.util.OptionalInt;
import java.util.function.IntBinaryOperator;
import java.util.stream.IntStream;

public class TestStream {
    public static void main(String[] args) {
        IntStream range = IntStream.range(0, 10);
        IntBinaryOperator ibo = new IntBinaryOperator() {
            @Override
            public int applyAsInt(int left, int right) {
                return left + right;
            }
        };
        int reduce = range.reduce(5, ibo);
        System.out.println(reduce);
    }
}
