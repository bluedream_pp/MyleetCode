package person.testSpliterator;

import java.time.temporal.ChronoField;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;
import java. util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * 题目：将字符串中的数字相加，并返回结果。
 * 例如： 输入：12sdfasdfas12 2131 121jsdfjjf121
 *        输出： 12 + 12 + 2131 + 121 + 121 = 2397
 * 解决的办法：
 * 使用 stream 的方式来解决。
* */
public class Solution2 {
    public int parsePlus(String in ){
        Stream<Character> characterStream = IntStream.range(0, in.length()).mapToObj(in::charAt);
        NumCounter reduce = characterStream.reduce(new NumCounter(), new MyAcculate() , NumCounter::combine);
        return reduce.getResualt();
    }
    public int parsePlus3(String in ){
        NumCounter rs = null ;
        NumCounterSpliterator spliterator = new NumCounterSpliterator(0,in.length()-1, 0 , in);
        Stream<Character> stream = StreamSupport.stream(spliterator, false);
        rs = stream.reduce(new NumCounter(), new MyAcculate() , NumCounter::combine);
        return rs.getResualt() ;
    }
    public int parsPlus2(String in ){
        NumCounter numCounter = new NumCounter();
        numCounter.setNum(0);
        numCounter.setSum(0);
        NumCounterSpliterator spliterator = new NumCounterSpliterator(0,in.length()-1, 0 , in);
        NumCounterConsumer ncc = new NumCounterConsumer(numCounter);
        while((spliterator = (NumCounterSpliterator) spliterator.trySplit()) != null){
            ncc.forSum(spliterator);
        }
        System.out.println(numCounter.getNum());
        return numCounter.getResualt();
    }
    public static void main(String[] args) {
        Solution2 s = new Solution2();
        System.out.println(s.parsePlus("12sdfasdfas12 2131 121jsdfjjf121"));
//        System.out.println(Character.isDigit("12sdfasdfas12 2131 121jsdfjjf121".charAt(2)));
    }
}
