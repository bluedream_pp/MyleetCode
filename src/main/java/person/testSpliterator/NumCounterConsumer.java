package person.testSpliterator;

import java.util.function.Consumer;

public class NumCounterConsumer implements Consumer<Character> {
    private NumCounter numCounter = null ;
    NumCounterConsumer(NumCounter numCounter){
        this.numCounter = numCounter;
    }

    public void forSum(NumCounterSpliterator s ){
        while(s.tryAdvance(this)){}
        numCounter.setSum(numCounter.getSum()+numCounter.getNum());
        numCounter.setNum(0);
    }

    @Override
    public void accept(Character c) {
        this.numCounter.setNum(Integer.parseInt("" + numCounter.getNum() + c));
    }
}
