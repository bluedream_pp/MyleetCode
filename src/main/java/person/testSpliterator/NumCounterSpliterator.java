package person.testSpliterator;

import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * @author wangyifei
 */
public class NumCounterSpliterator implements Spliterator<Character> {
    private String in ;
    private int currIdx = 0 ;
    private int start = 0;
    private int end = 0;

    NumCounterSpliterator( int start , int end , int currIdx, String in ){
        this.in = in ;
        this.currIdx = currIdx;
        this.start = start;
        this.end = end ;
    }
    @Override
    public boolean tryAdvance(Consumer<? super Character> action) {
        action.accept(in.charAt(start++));
        return start <= end;
    }
    @Override
    public Spliterator<Character> trySplit() {

        int start = 0 , end = 0 ;
        boolean first = true ;
        for(int i = currIdx ; i < in.length() ; i ++){
            //找到第一个数字
            if( first && Character.isDigit(in.charAt(i))){
                start = i ;
                first = false;
            }

            if(!first && Character.isDigit(in.charAt(i-1<0?0:i-1)) && !Character.isDigit(in.charAt(i))){
                System.out.println(start + (i-1));
                return new NumCounterSpliterator( start , i-1, i  ,in);
            }

        }
        if(!first){
            return new NumCounterSpliterator( start , in.length()-1, in.length()  ,in);
        }
        return null;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return 0;
    }
}
