package person.testSpliterator;
/**
 * 题目：将字符串中的数字相加，并返回结果。
 * 例如： 输入：12sdfasdfas12 2131 121jsdfjjf121
 *        输出： 12 + 12 + 2131 + 121 + 121 = 2397
* */
public class Main {
    private int parsePlus(String in ){
        int rs = 0 ;
        int temp = 0 ;
        char[] chars = in.toCharArray();
        for(char e : chars){
            if(e >= '0' && e <= '9'){
                temp = 10*temp + (e - '0');
            }else if( temp > 0){
                System.out.println(temp);
                rs += temp ;
                temp = 0 ;
            }
        }
        if(temp > 0){
            rs += temp ;
        }
        return rs ;
    }

    public static void main(String[] args) {
        Main m = new Main();
        // 打印出来的结果是 12 + 12 + 2131 + 121 + 121 = 2397
        System.out.println(m.parsePlus("12sdfasdfas12 2131 121jsdfjjf121"));
    }
}
