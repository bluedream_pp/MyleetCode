package person.testSpliterator;

/**
 * @author wangyifei
 */
public class NumCounter {
    private int sum = 0 ;
    private int num = 0 ;
    private char MIN = '0';
    private char MAX = '9';

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getSum() {
        return sum;
    }
    public int getResualt(){
        return sum + num ;
    }
    public NumCounter plus(Character c ){
        if(c > MIN && c < MAX){
            num = num*10 + c - '0';
        }else{
            System.out.println(num);
            sum += num ;
            num = 0 ;
        }
        return this;
    }
    public NumCounter combine(NumCounter c){
        System.out.println("combine" + num);
        sum += num ;
        return this ;
    }
}
