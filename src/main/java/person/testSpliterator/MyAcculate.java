package person.testSpliterator;

import java.util.function.BiFunction;

public class MyAcculate implements BiFunction<NumCounter , Character , NumCounter> {
    @Override
    public NumCounter apply(NumCounter numCounter, Character character) {
        // 假如 character 是一个数字，则将数字放到 numCounter.num 里面
        if(Character.isDigit(character)){
            numCounter.setNum(Integer.parseInt(""+numCounter.getNum()+character));
        }else{
            numCounter.setSum(numCounter.getNum() + numCounter.getSum());
            numCounter.setNum(0);
        }
        return numCounter;
    }
}
