package person.FP;

import java.util.function.Supplier;

public class MySupplier implements Supplier<String> {
    private int cnt = 0 ;
    private String[] array = {"James,male","Biden,male","Trume,male"};
    @Override
    public String get() {
        return (cnt < array.length)?array[cnt++]:null;
    }
}
