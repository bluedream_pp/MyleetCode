package person.FP;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class TestPredicate {
    public void filterString(Supplier<String> supplier , Predicate<String> pre1, Predicate<String> pre2, Predicate<String> pre3){
        String str = null ;
//        while( null != (str = supplier.get()) ){
//           if(pre1.negate().and(pre2.negate()).and(pre3.negate()).test(str)){
//               System.out.println(str);
//           }
//        }
        if(supplier instanceof MySupplier2){
            ((MySupplier2)supplier).reset();
        }
        while( null != (str = supplier.get()) ){
            if(pre1.and(pre2).and(pre3).test(str)){
                System.out.println(str);
            }
        }
    }
    public static void main(String[] args) {
        TestPredicate tp = new TestPredicate();
        tp.filterString(new MySupplier2()
                ,x-> {
                    return x.matches("good|brilliant|wonderful");
                }
                ,x -> !x.contains("fuck")
                ,x -> !x.contains("mother")
                );
    }
}
