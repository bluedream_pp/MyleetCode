package person.FP;

public class LAMBD {
    public static void main(String[] args) {
         LAMBD l = new LAMBD();
         l.callYangChao(new YangChao() {
             @Override
             public void say() {
                 System.out.println("hello world");
             }
         });
         l.callYangChao(() -> System.out.println("hello world"));

         l.callCook(x -> System.out.println(x));
        String s = l.callYangchao2(x -> {
            return x;
        });
        String s1 = l.callYangChao3(x -> x);
        System.out.println(s1);


    }
    public String callYangChao3(YangChao3<? super String> yc){
        return (String) yc.doSomething("nihk");

    }
    public String callYangchao2(YangChao2 yc){
        return yc.doSomething("做饭");
    }
    public void callCook(Cook cook){
        cook.cook("大螃蟹");
    }
    public void callYangChao(YangChao yc){
        yc.say();
    }
}
