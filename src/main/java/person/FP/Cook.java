package person.FP;
@FunctionalInterface
public interface Cook {
    public void cook(String what) ;
}
