package person.FP;

import java.util.function.Supplier;

public class MySupplier3 implements Supplier<String> {
    private String[] strs = {
            "sdf;1213asdfas",
            "毛 98",
            "fu12ck",
            "gooasd87d",
            "brilli876ant",
            "wonderf776ul",
    };
    private int cnt = 0 ;
    public void reset(){
        cnt = 0 ;
    }
    @Override
    public String get() {
        return cnt < strs.length?strs[cnt++]:null;
    }
}
