package person.FP;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class PrintStringFormat {
    public void printStringFormat(Supplier<String> supplier , Consumer<String> printName , Consumer<String> printSex){
        String str = null ;
        while((str = supplier.get()) != null){
            printName.andThen(printSex).accept(str);
        }
    }

    public static void main(String[] args) {
        PrintStringFormat psf = new PrintStringFormat();
        int cnt = 0 ;
       psf.printStringFormat( new MySupplier()
        , (String x ) -> {
              System.out.print("姓名："+x.split(",")[0]);
        },(String x ) -> {
              System.out.println("。性别："+x.split(",")[1]+"。");
        });
    }

}
