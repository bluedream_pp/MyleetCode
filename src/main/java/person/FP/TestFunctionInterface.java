package person.FP;

import java.util.function.Function;
import java.util.function.Supplier;

public class TestFunctionInterface {
    /**
     * f1 负责将字符里面的非数字去掉。f2 负责将字符串转成整型。
     * */
    public void str2Int(Supplier<String> supplier
                      , Function<String ,String> f1
                      , Function<String,Integer> f2
                      , Function<Integer,String> f3 ){
        String next = null ;
        while(null != (next = supplier.get())){
            System.out.println(f1.andThen(f2).andThen(f3).apply(next));
        }
    }

    public static void main(String[] args) {
        TestFunctionInterface t = new TestFunctionInterface();
        t.str2Int(new MySupplier3()
                ,x-> x.replaceAll("\\D+","")
                ,x-> Integer.valueOf(x)
                ,x-> x+"*100 = " + x*100
        );
//        System.out.println("sdf;1213asdfas".replaceAll("\\D+",""));
    }
}
