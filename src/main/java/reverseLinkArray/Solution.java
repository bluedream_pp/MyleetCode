package reverseLinkArray;

public class Solution {
    public void reverse(ListNode head){
        ListNode[] list = new ListNode[100];
        int idx = 0 ;
        ListNode next = head ;
        do{
            list[idx++] = next;
        }while ( (next = next.getNext() )!= null);
        idx = idx -1 ;
        head.setNext(null);
        head = list[idx] ;
        for(int i = idx ; i > 0;i--){
            list[i].setNext(list[i-1]);
        }
        forLinkList(head);
    }
    public void reverse2(ListNode head){
        ListNode last = null ;
        ListNode temp = null ;
        ListNode curr = head;
        while(curr != null){
            temp = curr.getNext();
            curr.setNext(last);
            last = curr ;
            curr = temp;
        }
        forLinkList(last);
    }
    public void forLinkList(ListNode head){
        ListNode next = head;
        do{
            System.out.println(next.getVal());
        }while((next = next.getNext()) != null);

    }
}
