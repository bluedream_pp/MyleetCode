package lastRemaining;

public class Child {
    private int childID ;
    private Child next;

    public Child getPre() {
        return pre;
    }

    public void setPre(Child pre) {
        this.pre = pre;
    }

    private Child pre;

    public Child(int childID) {
        this.childID = childID;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public Child getNext() {
        return next;
    }

    public void setNext(Child next) {
        this.next = next;
    }
}
