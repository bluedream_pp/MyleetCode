package persion.leetcode.longestPalindrome;

/**
 * Created by wangyifei on 19/7/19.
 * desc: 使用蛮力的方法，就是把所有子字符串遍历出来后，进行匹配是否是回文
 */
public class longestPalindrome {


    public static void main(String[] args) {

        System.out.println(lazy("baeab"));

    }

    public static String lazy(String str ){
       String rs = "" ;
       int strLength = 0 ;


       char[] chars = str.toCharArray();
       String temp = "" ;
       for(int i = 0 ; i < str.length() ; i++){
           temp =  String.valueOf(chars[i]);
          for(int j = i + 1 ; j < str.length() ; j++) {
              temp = temp + chars[j] ;
              if(isPalindrome(temp) && strLength < temp.length()){
                  strLength = temp.length();
                  rs = temp;

              }
          }
       }

       return "the maxinum palindrom is:" + rs ;
    }


    /*
    *desc: detect weather str is a palindrome
    * */
    public static boolean isPalindrome(String str){

        boolean rs = true ;


        // the length of str is even
        char[] chars = str.toCharArray();

        for(int i = 0 ; i < str.length()/2;i++) {
          if(chars[i] != chars[str.length()-1-i]){
             rs = false ;
             break;
          }
        }


         return rs;
    }

}
