package persion.leetcode.MaxlengthStr;

public class MyBitSet {
   private long bitSet = 0L ;
   
   public boolean get(Integer number) {

	 long exchageBinary = 1L << number ; 
	 
	 return  (exchageBinary & bitSet) == exchageBinary;
   }
   public void set(Integer put){
	   bitSet = bitSet | (1L << put);
   }
   public long getBitSet(){
	   return bitSet ;
   }
   
}
