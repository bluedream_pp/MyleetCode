package persion.leetcode.MaxlengthStr;
import java.util.Arrays;

import static persion.leetcode.common.commonUtil.println;
/**
 * Created by wangyifei on 19/6/1.
 */
public class MaxlengthStr3 {

    public static void main(String[] args) {
        println(getMaxNorepeatLength("iuujhh"));
    }
    public static int getMaxNorepeatLength(String str){

        int[] freg =new int[256];
        char[] chars = str.toCharArray();
        // L dedicate the beginning index of str
        // R dedicate the end index of str
        int rs = 0 ;
        int L = 0 ;
        int R = -1 ;
        // 因为 0 代表了数组的第一个元素的下标值，所以统一使用 -1 作为字符出现位置的初始值
        Arrays.fill(freg,-1);
        // 先试 R+1 是否越界，如果没有越界，那么可以做窗口的操作了。
        while(R + 1 < chars.length) {
            R++ ;
            // 需要先判断当前字符是否出现过，然后在把当前字符的位置放入到 chars 里面才行
            if(-1 != freg[chars[R]]){
                L =  Math.max(L , freg[chars[R]]);
            }
            // 先记录上最后字符出现的位置
            freg[chars[R]] = R ;
            rs = Math.max(rs , R - L + 1);
        }

        return rs ;
    }
}
