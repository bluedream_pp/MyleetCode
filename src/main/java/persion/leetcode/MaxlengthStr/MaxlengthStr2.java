package persion.leetcode.MaxlengthStr;

import static persion.leetcode.common.commonUtil.println;

public class MaxlengthStr2 {

   public static void main(String[] args){
        println(getMaxNorepeatLength("iuujhh"));
   }
  public static int getMaxNorepeatLength(String str){

      int[] freg =new int[256];
      char[] chars = str.toCharArray();
      // L dedicate the beginning index of str
      // R dedicate the end index of str
      int rs = 0 ;
      int L = 0 ;
      int R = -1 ;
      // 先试 R+1 是否越界，如果没有越界，那么可以做窗口的操作了。
      while(R + 1 < chars.length) {
          if( 0 == freg[chars[R+1]]){
            freg[chars[++R]]++ ;
          }else{
            freg[chars[L++]]-- ;
          }
        rs = Math.max(rs , R - L + 1);
      }

       return rs ;
  }
}
