package persion.leetcode.MaxlengthStr;
import static persion.leetcode.common.commonUtil.println;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
public class MaxlengthStr {

   public static void main(String[] args){
	   /*
	   *
	   *我的程序里面有一个 BUG ，当 tail 向前移动的时候，需要将进入到 char[254] 的字符出现的次数减掉，否则会影响
	    * 下一个最长无重复字符的查询操作
	   * */
	   for(String s : findMaxLengthStr("qwe123")) {
		   println(s);
	   }

	  for(String s : findMaxLengthStr("abcabcbb")) {
		   println(s.length());
		   println(s);
	   }
	   /*
	    * 本来想把最长的字符串输出出来呢，但是远比我想象的复杂了，下次可以尝试一下
	    * */
   } 
   
   
  public static List<String> findMaxLengthStr(String target) {
	String maxStr = "" ;
	String tempMaxStr= null ;
    List<String> rsList = new ArrayList<String>();
    BitSet bitset = new BitSet();
	Integer windowHeader = 0 ; 
	Integer windowTail = 0 ; 
	char[] chars = target.toCharArray(); 
	Integer charNumber = null ;
   
	for(int i = 0 ; windowHeader < chars.length;) {
		if(windowHeader < chars.length ){
			i = windowHeader ;
			windowHeader ++;
		}
//		else{
//			i = windowTail ;
//			windowTail ++ ;
//		}
        charNumber = Integer.valueOf(chars[i]);
		if(bitset.get(charNumber)){
//          tempMaxStr =  getSubString(windowTail ,windowHeader-2 , chars);
//          if(maxStr.length() < tempMaxStr.length()) {
//        		  maxStr = tempMaxStr;
//          }
		   windowTail = moveHeader(chars[i] , chars);
		}else{
			bitset.set(charNumber);
			// todo:  expand the window size by plus one to windowTail
			maxStr = maxStr + String.valueOf(chars[i]);
		}
	}
	rsList.add(maxStr);
	 return rsList; 
  }
  public static String getSubString(int tail , int header  , char[] chars){

	 StringBuilder sb = new StringBuilder(); 
	 for(int i =  tail ; i <= header ; i ++){
		 sb.append(chars[i]);
	 }
	 return sb.toString();
  }
  public static int moveHeader(char a ,char[] chars ){
	 int index = 0 ;
	 
	 for(int i =0 ; i < chars.length; i++){
		if(chars[i] == a ) {
			index = i ;
			break ; 
		}
	 }
	 return index + 1;
  }
}
