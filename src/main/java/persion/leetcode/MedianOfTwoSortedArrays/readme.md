## 大体思路

抓住两点：

- 两个数组是有序的
- 中位数位置的特殊性，也就是在数列的中间

由于中位数再数列的中间，假设两个数组为 A、B，中位数所在的位置是 K，中位数左边的数组为 S ，S 是有 A、B 两个数组的子
数组组成的，再假设两个子数组的最大下标是 m、n。那么 m + n = K 。K = (length(A)+length(B))/2 这是显而易见的。
但是呢，m 和 n 的组合是有很多的，而且 m 和 n 的组合是有规律的：有序性。


[好资源1](https://www.jianshu.com/p/7c990b12a82a)

[好资源2](https://www.cnblogs.com/codernie/p/9180361.html#jump4)

[好资源2](https://github.com/haoel/leetcode/blob/master/algorithms/cpp/medianOfTwoSortedArrays/medianOfTwoSortedArrays.cpp)

这个讲的比较透彻，从数学的角度把算法解释通了。


