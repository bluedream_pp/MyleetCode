package power;

public class Solution {
    private double power(double base , int exponent){
        double rs = 1D;
        int a = 0 ;
        if(exponent < 0){
            base = 1/base;
            exponent *= -1 ;
        }
        while(a<exponent){
            rs *= base;
            a++;
        }
        return rs ;
    }
    private double power2(double base , int exponent){
        double rs = 1D;
        double x = 1D ;
        if(exponent < 0){
            base = 1/base;
            exponent *= -1 ;
        }
        while(exponent != 0 ){

            x *= base;
            if((exponent&1) == 1){
                rs = rs * x ;
            }
            exponent >>= 1;
        }

        return rs ;
    }
    public static void main(String[] args) {

        System.out.println(new Solution().power(2D,-3));
        System.out.println(new Solution().power2(2D,-3));
        System.out.println(new Solution().power2(2D,3));

    }
}
