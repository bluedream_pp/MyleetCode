package pockPlay;
/**
*LL今天心情特别好,因为他去买了一副扑克牌,发现里面居然有2个大王,2个小王(一副牌原本是54张^_^)...他随机从中抽出了5张牌,
 * 想测测自己的手气,看看能不能抽到顺子,如果抽到的话,他决定去买体育彩票,嘿嘿！！“红心A,黑桃3,小王,大王,方片5”,“Oh My God!”不是顺子.....
 * LL不高兴了,他想了想,决定大\小 王可以看成任何数字,并且A看作1,J为11,Q为12,K为13。上面的5张牌就可以变成“1,2,3,4,5”(大小王分别看作2和4),“So Lucky!”。
 * LL决定去买体育彩票啦。 现在,要求你使用这幅牌模拟上面的过程,然后告诉我们LL的运气如何， 如果牌能组成顺子就输出true，否则就输出false。为了方便起见,你可以认为大小王是0。
 * 我的解题思路是：
 * 先把大小王都弄到集合 A 里面。再把非大小王弄到集合 B 里面。
 * 然后对，A 做升序，然后遍历，A 里面的元素
 * 1. 如果 A[i] - A[i] < B的元素个数，则 B的元素个数 - （A[i] - A[i]）
 * 2. 继续循环，如果 count 的值小于 0 ，则说明，大小王不能补位
* */
public class Solution {
    private int findMin(int[] array , int start , int end ){
        int minIdx = start ;
        for(int i = start ; i <= end ;i++){
            if(array[minIdx]> array[i]){
                minIdx = i;
            }
        }
        return minIdx ;
    }
    private void swap(int[] array , int x , int y){
        int swap = array[x];
        array[x] = array[y];
        array[y] = swap ;
    }
    private void sort(int[] array , int start , int end ){
        for(int i = start ; i <= end ; i++){
            int swapPoint = findMin(array ,i ,end) ;
            swap(array , i , swapPoint);
        }
    }
    private boolean isContinuous(int[] numbers){
        int[] kings = new int[100];
        int[] nonKings = new int[100];
        int kingIdx = 0 ;
        int nonKingIdx = 0 ;
        for(int e : numbers){
            if( 0 == e){
                kings[kingIdx++] = e ;
            }else{
                nonKings[nonKingIdx++] = e ;
            }
        }
        sort(nonKings , 0 , nonKingIdx-1);
        int count =  kingIdx ;
        for(int i = 0 ; i < nonKings.length-1 ; i++){
            if(  nonKings[i+1] - nonKings[i] - 1 <= count ){
                if(nonKings[i+1] - nonKings[i] > 1 ){
                    count = count - (nonKings[i+1] - nonKings[i] - 1) ;
                }
            }else{
                return false;
            }
        }
        if(count < 0 ){
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        int[] a = {1,7,8,0,0,2};
        System.out.println(new Solution().isContinuous(a));
    }
}
