package sumNumbers;
/**
 * 写一个函数，求两个整数之和，要求在函数体内不得使用+、-、*、/四则运算符号。
 * 9:1001
 * 1:0001
 * 1 ^ 9 = 1000
 * 1 & 9 = 0001
 * 1000 | 0010 = 1010
 * 1000 & 0010 = 0000
 * 结束
 *
 *15： 1111
 * 1：0001
 * 15 ^ 1 = 1110
 * 15 & 1 = 0001
 *
 * 1110 ^ 0010 = 1100
 * 1110 & 0010 = 0010 > 0 则要再次进行移位
 *
 * 1100 ^ 0100 = 1000
 * 1100 & 0100 = 0100
 *
 * 1000 ^ 1000 = 1000
 * 1000 & 1000 = 1000
 *
 * 1000 ^ 10000 = 11000
 * 1000 & 10000 = 00000
 *
* */
public class Solution {
    public long sum(int a , int b ){
        long rs ;
        long rs2 = 0;
        long mid ;
        long mid2 =0;
        rs2 = rs = a ^ b ;
        mid2 = mid = a & b ;
       // 如果 a & b > 0 说明还有进位的位数

        while(mid > 0){
            // mid> 0 说明需要进位，
            rs2 = rs ;
            mid<<=1;
            rs = rs ^ (mid);
            mid =(rs2 & mid);
        }
        return rs ;
    }
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.sum(15,1));
        System.out.println(solution.sum(1,15));
        System.out.println(solution.sum(3,-3));
        System.out.println(solution.sum(-1,19));
        System.out.println(solution.sum(9,0));
        System.out.println(solution.sum(-1,-2));
    }
}
