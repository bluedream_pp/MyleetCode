package twoStackEqualQueue;

import java.util.Stack;

public class Solution {
    private Stack<Integer> stack1 = new Stack<>();
    private Stack<Integer> stack2 = new Stack<>();
    public int pop(){
        int rs = -1 ;
        while(!stack1.empty()){
            stack2.push(stack1.pop());
        }
        rs = stack2.pop();
        while (!stack2.empty()){
            stack1.push(stack2.pop());
        }
        return rs;
    }
    public void push(int i){
        stack1.push(i);
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        s.push(1);
        s.push(2);
        s.push(3);
        s.push(4);
        s.push(5);
        s.push(6);
        System.out.println(s.pop());
    }
}
