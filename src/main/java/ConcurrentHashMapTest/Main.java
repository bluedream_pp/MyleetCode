package ConcurrentHashMapTest;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    public static void main(String[] args) {
        Map<Integer,String> map = new ConcurrentHashMap<>(29);
        new Thread(()->{
            map.put(new Integer(1) , "bb");
        },"t1").start();
        map.put(new Integer(1) , "aa");
        map.get(new Integer(1));
    }
}
