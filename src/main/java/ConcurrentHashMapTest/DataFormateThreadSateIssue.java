package ConcurrentHashMapTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DataFormateThreadSateIssue {
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static void test() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Future<?>> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Future<?> submit = executorService.submit(() -> {
                String rs = null ;
//                rs = simpleDateFormat.parse("2022-09-22").toString();
                rs = LocalDate.parse("2022-09-22", DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString();
                return rs ;
            });
            list.add(submit);
        }
        for (Future<?> future : list) {
            System.out.println(future.get());
        }
        executorService.shutdown(); ;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        test();
    }
}

