package ConcurrentHashMapTest;

import java.util.Random;

public class WordGenerator {
    private final static char a = 'a';
    private final static char z = 'z';

    private final static char A = 'A';
    private final static char Z = 'Z';
    public static String getWord(){
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        int i =0 ;
        int rs = 0 ;
        for(int j =0 ; j < 5 ; j++){
            i = r.nextInt('z' - 'a');
            rs = (int)(( i%2 == 0 ? 'a':'A' ) + i );
            sb.append((char)rs);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(WordGenerator.getWord());
    }
}
