package ConcurrentHashMapTest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class RandomMultiThreadIssum {
    private static ThreadLocalRandom random = ThreadLocalRandom.current();
    public static void test(){
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10; i++) {
            executorService.submit(()->{
                random.nextInt(10);
                System.out.println(ThreadLocalRandom.current());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
    }
    public static void main(String[] args) {

        test();

    }
}
