package BinaryTreeMirror;

public class Main {
    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        TreeNode t5 = new TreeNode(5);
        TreeNode t6 = new TreeNode(6);
        TreeNode t7 = new TreeNode(7);
        TreeNode t8 = new TreeNode(8);
        t1.setLeft(t2);
        t1.setRight(t3);

        t2.setLeft(t4);
        t2.setRight(t5);

        t3.setLeft(t6);
        t3.setRight(t7);
        Solution solution = new Solution();
        solution.mirror3(t1);
        solution.forTree(t1);
    }
}
