package BinaryTreeMirror;

import sun.reflect.generics.tree.Tree;

import java.util.ArrayList;

public class Solution {
    public void mirror(TreeNode head){

        if(null == head){
            return ;
        }

        swap(head);
        mirror(head.getLeft());
        mirror(head.getRight());
    }


    public void mirror2( TreeNode head){
        ArrayList<TreeNode> list = new ArrayList<>();
        list.add(head);
        while(!list.isEmpty()){
            swap(list.get(0));
            if(list.get(0).getLeft() != null){
                list.add(list.get(0).getLeft());
            }
            if(list.get(0).getRight() != null ){
                list.add(list.get(0).getRight());
            }
            list.remove(0);
        }
    }
   public void mirror3(TreeNode head){
        TreeNode[] tree = new TreeNode[100];
        int treesLength = 0 ;
        tree[treesLength++] = head ;
        int treeIdx = 0 ;
        while(treeIdx <= treesLength-1){
            swap(tree[treeIdx]);
            if(tree[treeIdx].getLeft() != null){
                tree[treesLength++] = tree[treeIdx].getLeft();
            }

            if(tree[treeIdx].getRight() != null){
                tree[treesLength++] = tree[treeIdx].getRight();
            }
            treeIdx++;
        }

   }
    public void mirror4(TreeNode head){
        TreeNode left = null ;
        TreeNode right = null ;
    }
    public void swap(TreeNode node){
        TreeNode swap = null ;
        swap = node.getLeft();
        node.setLeft(node.getRight());
        node.setRight(swap);
    }
    public void forTree(TreeNode head){
       if(head == null ) {
           return ;
       }
        System.out.println(head.getVar());
        forTree(head.getLeft());
        forTree(head.getRight());

    }
}
