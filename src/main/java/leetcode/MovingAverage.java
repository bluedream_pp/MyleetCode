package leetcode;
class MovingAverage {
    int capacity, cursor, sum, cnt;
    int[] array;

    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        this.capacity = size;
        this.array = new int[size];
    }

    public double next(int val) {
        sum += (val - array[cursor % capacity]);
        array[cursor++ % capacity] = val;
        cnt++;
        return (double) sum / Math.min(cnt, capacity);
    }

    public static void main(String[] args) {
        MovingAverage movingAverage = new MovingAverage(2);
        System.out.println(movingAverage.next(4));
        System.out.println(movingAverage.next(0));
    }
}