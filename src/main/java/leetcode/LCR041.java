package leetcode;


import java.util.Arrays;

/**
 * @className: LCR041
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/5/8 8:16
 */
public class LCR041 {
    public static void main(String[] args) {
//        String[] inputs = {"MovingAverage", "next", "next", "next", "next"};
//        String[] inputs = [[3], [1], [10], [3], [5]];
        MovingAverage movingAverage = new MovingAverage(1);
        System.out.println(movingAverage.next(4));
        System.out.println(movingAverage.next(0));
    }
    public static class MovingAverage{
        private int size ;
        private int[] sum ;
        private int start ;
        private int end ;
        public MovingAverage(int size){
            this.size = size ;
            this.sum = new int[50];
        }
        public double next(int val){
            if(start == 0){
                sum[start] = val ;
            }else{
                sum[start] = sum[start-1] + val;
            }
            double ans = (double)(end - 1 >= 0 ? (sum[start] - sum[end -1]) :  sum[start]) / (double) (start - end + 1);
            if(start - end >= size -1){
                end++ ;
            }
            start++;
            return ans ;
        }
    }
}
