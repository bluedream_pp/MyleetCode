package rectCover;
/**
* 我们可以用2*1的小矩形横着或者竖着去覆盖更大的矩形。请问用n个2*1的小矩形无重叠地覆盖一个2*n的大矩形，总共有多少种方法？
* */
public class Solution {
    public static void main(String[] args) {
        System.out.println(new Solution().rectCover(4));
    }
    private int rectCover(int n){
        if(n<1){
            System.out.println("invalidate input n");
        }
        int temp = 0 ;
        int a = 1 ;
        int b = 2 ;
        for(int i = 3 ; i<=n;i++){
            temp = b ;
            b = a+b;
            a=temp;
        }

        return b ;
    }
}
