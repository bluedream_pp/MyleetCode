package Fundamental.collectionTest;


import java.util.*;

public class CollectionTest {
    public static void main(String[] args) {
        List<Circle> circles = new ArrayList<>();
        Circle circle = null ;
        Random r = new Random();
        int i = 0 ;
        while (i<10) {
            circle = new Circle();
            circle.setBanjing(r.nextDouble()*100);
            circles.add(circle);
            i++;
        }
        System.out.println();
        circles.stream().forEach(x->{
            System.out.println(x);
        });
        // 使用 comparable 进行比较。
        Collections.sort(circles);

        System.out.println("----------------------------------------");
        circles.stream().forEach(x->{
            System.out.println(x);
        });

        // 采用 Comparator 进行比较
        Collections.sort(circles, new Comparator<Circle>() {
            @Override
            public int compare(Circle o1, Circle o2) {
                return (int)(o2.getBanjing() - o1.getBanjing());
            }
        });

        System.out.println("----------------------------------------");
        circles.stream().forEach(x->{
            System.out.println(x);
        });
    }
    public static class Circle implements Comparable<Circle>{
        private double banjing;

        @Override
        public String toString() {
            return "Circle{" +
                    "banjing=" + banjing + " round : "  + Math.round(banjing) +
                    " ceil:" + Math.ceil(banjing)+
                    " floor:" + Math.floor(banjing)+
                    '}';
        }

        public double getBanjing() {
            return banjing;
        }

        public void setBanjing(double banjing) {
            this.banjing = banjing;
        }

        @Override
        public int compareTo(Circle o) {
            return (int)(this.banjing - o.getBanjing());
        }
    }
}
