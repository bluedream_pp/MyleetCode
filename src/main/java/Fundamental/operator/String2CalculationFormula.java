package Fundamental.operator;

import java.util.HashMap;
import java.util.Objects;

public class String2CalculationFormula {
    /**
     * 使用 hashMap 来存储运算符号，这样可以加快查询速度。
     * 这还可以使用 bitSet 来更快速的查找。hashSet 也是可以的
     * */
    private static HashMap<Integer,Character> operators = new HashMap<>();
    private static HashMap<Integer,Character> solooperators = new HashMap<>();
    static {
        operators.put((int)'+' , '+');
        operators.put((int)'-' , '-');
        operators.put((int)'*' , '*');
        operators.put((int)'/' , '/');
        operators.put((int)'~' , '~');
        operators.put((int)'>' , '>');
        operators.put((int)'<' , '<');
        solooperators.put((int)'+' , '+');
        solooperators.put((int)'-' , '-');
        solooperators.put((int)'~' , '~');
    }
    private static CharacterType whichType(Character c ){
        if(Objects.isNull(operators.get(c))){
            return CharacterType.OPERATER;
        }
        return CharacterType.NUMBER;
    }
    /**
     * 使用数组记录下所有运算符号的位置，然后计算符号中数字解析出来。
     * */
    public static int transformationTagOperator(String formula ){
        int rs = 0 ;
        return rs ;
    }
    /**
     * 使用栈的方式解析出计算符号
     * */
    public static int transformationWithStack(String formula ){
        int rs = 0;
        return rs ;
    }
    /**
     * 解析出一个运算符号，一个数字，将所有的运算符号都作用到数字，然后将结果都加到结果里面。
     * */
    public static int transformation(String formula ){
        int rs = 0 ;
        char[] chars = formula.toCharArray();
        CharacterType preType = whichType(chars[0]);
        CharacterType curType = null ;
        Character character = null ;
        int operatorNum = 0 ;
        char operator = '(' ;
        // 运算符
        for (char currChar : chars) {
            System.out.println(currChar);
            character = operators.get((int)currChar);
            // 如果 character 是空的，说明是数字
            if(Character.isSpaceChar(currChar)){
                continue;
            }
            if (!Objects.isNull(character)){
                // 是计算符号
                if(currChar == '(' && operatorNum > 0){
                    rs = calculate(operatorNum ,'+');
                }else if(currChar != '(' && operatorNum > 0){
                    rs = rs + calculate(operatorNum , operator);
                }
                operator = currChar;
                operatorNum = 0;
            } else {
                operatorNum =  10*operatorNum + Integer.parseInt(""+currChar);

            }
        }

        rs = rs + calculate(operatorNum , operator);

        return rs;
    }

    private static int calculate(int a , char operator){
        switch (operator){
            case '+':
                return a;
            case '-':
                return -a;
            case '~':
                return ~a ;
            default:
                return a;
        }
    }
    private static void run(String num){
        int transformation = transformation(num);
        System.out.println(String.format(" the result of %s is %s ", num , transformation));
    }


    public static void main(String[] args) {
//        run("123+123");
        run("-123+123");
    }
}

enum CharacterType{
    /**
     * */
    OPERATER("operator"),
    NUMBER("number"),
    ;
    private String type ;

    /**
     *
     * */
    CharacterType(String type ){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
}
class OperatorNumber{
    public int num ;

    public OperatorNumber(int num, boolean state) {
        this.num = num;
        this.state = state;
    }

    /**
     * true 代表 num 已经拼接完成了， 例如 123 + 11 - 2 ,
     * 当走到 - 的时候，11 这个数字数拼接完成了。
     * */
    public boolean state ;

}

