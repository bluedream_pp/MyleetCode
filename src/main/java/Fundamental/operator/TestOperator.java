package Fundamental.operator;

public class TestOperator {
    public static void main(String[] args) {
        int i = 1 ;
        /**
         *int 类型是 32 位的
         * 它的而进制格式位：  1: 0000 0000 0000 0000 0000 0000 0000 0001
         *                  ~1: 1111 1111 1111 1111 1111 1111 1111 1110
         *                  ~1-1： 1111 1111 1111 1111 1111 1111 1111 1101
         *                  ~1-1 取反： 1000 0000 0000 0000 0000 0000 0000 0010
         *                                                         0010
         *  ~1后是一个负数的补码：按照补码的公式，取反加1的规则。补码 = 取反 + 1
         *  己住公式就是 ~n =-n-1
         * */
        /**
         * 负数的二进制是补码，所以它位运算比较不好弄。
         * -2 的补码表示位 1111 1111 1111 1111 1111 1111 1111 1110
         * */
        to32Binary(Integer.MIN_VALUE , Integer.SIZE);
//        to32Binary(-2>>>1 , Integer.SIZE);
//        to32Binary(-2>>1 , Integer.SIZE);
//        to32Binary(-2 , Integer.SIZE);
//        to32Binary(~1 , Integer.SIZE);

    }
    /**
     * todo: 解析字符串后，拼接成运算公式，并计算。
     * preChar 和 curChar 的关系来判断运算符号和数字。
     * */

    public static void to32Binary(int number , int ws){
        System.out.println(String.format("十进制的表示为：%s", number) );
        String num = Integer.toBinaryString(number);
        int d = num.toCharArray().length - ws;
        StringBuilder sb = new StringBuilder();
        char zero = '0';
        for (int i = 0; i < -d ; i++) {
            sb.append(zero);
        }
        sb.append(num);
        System.out.println(String.format("二进制补码表示为:%s",sb.toString()));
        System.out.println();
    }
}
