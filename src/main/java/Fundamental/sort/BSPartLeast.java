package Fundamental.sort;

import java.util.Objects;

public class BSPartLeast {
    public static void main(String[] args) {
        int[] input = { 1  ,3 ,9 , 2 , 10};
        for (int i : randomArray(10, 100)) {
            System.out.print(i + " ");
        }
    }
    public static int oneMinIndex(int[] input){
        if(Objects.isNull(input) || input.length == 0){
            return -1 ;
        }
        if(input.length == 1){
            return 0 ;
        }
        return -1 ;
    }
    public static int[] randomArray(int maxLen , int maxValue){
        int len =(int)(Math.random()*maxLen) ;
        int[] ans = new int[len] ;
        int cnt = 0 ;
        int target = -1 ;
        int pre = -1 ;
        ans[0] = pre = (int)(Math.random()*maxValue);
        cnt++;
        int maxIndex = len -1;
        while(cnt < maxIndex) {
            target = (int)(Math.random()*maxValue);
            if(pre != target){
                ans[cnt] = target ;
                cnt++;
            }
        }
        return ans ;
    }
}
