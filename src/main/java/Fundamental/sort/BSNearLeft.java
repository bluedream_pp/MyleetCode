package Fundamental.sort;

import java.security.spec.RSAOtherPrimeInfo;
import java.util.Objects;

/**
 * @return:
 * @desc: 找到有序数组中，大于某个数最左边的下标值
 * @author
 * @date
 * @param
 */
public class BSNearLeft {
    public static void main(String[] args) {
        int[] input = { 1 , 2 , 2 , 3 , 8 , 99 , 100};
        solution(input , 3);
    }
    public static void solution(int[] input , int target){
        if(Objects.isNull(input) || input.length == 0){
            System.out.println(-1);
        }
        int L = 0 ;
        int H = input.length - 1 ;
        int M = 0 ;
        int rs = 0 ;
        while(L <= H){
            M = (L + H)/2;
            // 这不能是等号，
            // 如果是 input[M] 在 P2 的点上，则 L = M + 1 ， 就会错过最右的位置。
            //    /P2————P1————
            //  /
            if(target > input[M]){
                L = M + 1 ;
            }else{
                H = M - 1;
                rs = M ;
            }
        }
        System.out.println("result is:" + rs);
    }
}
