package Fundamental.sort;

import java.util.Arrays;

public class SelectSort {
    private int[] array ;
    public SelectSort(int[] array){
        this.array = array;
    }
    public void sort(){
        int minIndex = 0 ;
        int temp = 0 ;
        for (int i = 0; i < this.array.length - 1 ; i++) {
            temp = array[i];
            minIndex = i+1;
            for(int j = i+1 ; j < array.length ; j++){
                if(temp > array[j]){
                    minIndex = j ;
                    temp = array[j];
                }
            }
            if(temp < array[i]){
                swap(minIndex , i);
            }
        }
    }

    private void swap(int minIndex, int i) {
        int temp = 0 ;
        temp = array[minIndex];
        array[minIndex] = array[i];
        array[i] = temp;
    }

    @Override
    public String toString() {
        return "SelectSort{" +
                "array=" + Arrays.toString(array) +
                '}';
    }

    public static void main(String[] args) {
        SelectSort ss = new SelectSort(new int[]{1, 22, 11, 23, 32, 43});
        System.out.println("the original order:");
        System.out.println(ss.toString());
        ss.sort();
        System.out.println("the ordered array:");
        System.out.println(ss.toString());
    }
}
