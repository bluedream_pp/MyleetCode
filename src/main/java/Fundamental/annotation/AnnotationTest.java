package Fundamental.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationTest {
    @WAnnotation(from = "0" , to = "100")
    public void validate(int i){
        System.out.println(i);
    }
    public static void main(String[] args) {
        try {
            Class<?> clazz = Class.forName("Fundamental.annotation.AnnotationTest");
            for (Method method : clazz.getMethods()) {
//                for (Annotation annotation : method.getDeclaredAnnotations()) {
//                    System.out.println(annotation);
//                }
                if(method.isAnnotationPresent(WAnnotation.class)){
                    WAnnotation annotation = method.getAnnotation(WAnnotation.class);
                    System.out.printf("from is %s , to is %s" , annotation.from() , annotation.to());
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
