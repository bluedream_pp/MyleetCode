package Fundamental;


/**
 * @className: DivisionAndBit
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/19 9:18
 */
public class DivisionAndBit {
    public static void main(String[] args) {
        int i = 1000 ;
        System.out.println(i%11);
        // 1011
        System.out.println(i & 11);
    }
}
