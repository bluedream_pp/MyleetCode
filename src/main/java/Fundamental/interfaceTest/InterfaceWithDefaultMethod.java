package Fundamental.interfaceTest;

public interface InterfaceWithDefaultMethod {
    /**
     * 这个方法比较特殊，它是有方法体的，而且前面使用了 default 修饰。
     * 这是 1.8 之后的特性。
     * 在 1.8 之前，如果我们为一个接口增加了一个方法，那么它的实现类都要实现这个方法。
     * 但是到了 1.8 之后，实现类不必实现接口种的方法了，我们可以使用 default 类型的方法，如果
     * 实现类不实现抽象方法，就使用默认的实现。
     * */
    default void fun(){
        System.out.println("fun");
    }
}
