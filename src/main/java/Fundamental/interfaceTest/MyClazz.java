package Fundamental.interfaceTest;

public class MyClazz implements InterfaceWithDefaultMethod {
    public static MyClazz clazz = new MyClazz();
    {
        /**
         *构造块在 new 对象的时候执行。
         * */
        System.out.println("构造块");
    }
    static {
        /**
         *  静态块，在编译器加载类的时候就执行。
         * */
        System.out.println("静态块");
    }
    /**
     *再构造块之后执行。
     * */
    public MyClazz(){
        System.out.println("构造函数");
    }
    /**
     * 一个 Java 程序应该先编译，然后从 main 开始执行，所以
     * 静态块 > main > 构造语句块 > 构造函数
     * */
    public static void main(String[] args) {
        MyClazz myclazz = new MyClazz();
        myclazz.fun();
    }
}
