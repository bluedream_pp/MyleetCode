package Fundamental.collection;

import java.util.HashSet;
import java.util.Objects;

public class HashSetTest {
    public static void main(String[] args) {
        HashSet<Employee> hs = new HashSet<>();
        Employee james = new Employee("james", 23, new WDate(1987, 12, 9));
        hs.add(james);
        hs.add(new Employee("james", 23, new WDate(1987, 12, 9)));
        hs.forEach( x -> System.out.println(x));

    }
}
class Employee{
    private String name ;
    private int age ;
    private WDate birthday ;

    public Employee(String name, int age, WDate birthday) {
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", birthday=" + birthday +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }


        Employee employee = (Employee) o;
        if(!this.name.equals(employee.name) || this.age != employee.age || !employee.birthday.equals(this.birthday)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name,age,birthday.getYear() , birthday.getMonth() , birthday.getDay());
    }
}
class WDate{
    private int year ;
    private int month ;
    private int day;

    public WDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WDate wDate = (WDate) o;
        return year == wDate.year &&
                month == wDate.month &&
                day == wDate.day;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day);
    }

    @Override
    public String toString() {
        return "WDate{" +
                "year=" + year +
                ", month=" + (month<10?"0"+month:month) +
                ", day=" + (day <10?"0"+day:day) +
                '}';
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}

