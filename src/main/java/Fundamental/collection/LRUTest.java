package Fundamental.collection;


import java.util.LinkedHashMap;
import java.util.Map;

public class LRUTest<K,V> extends LinkedHashMap<K,V>{
    private int capacity ;

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() >= capacity;
    }

    public LRUTest(int capacity){
        super(capacity , 1.0F , true);
        this.capacity = capacity;
    }

    public static void main(String[] args) {
        LRUTest<String , String> lru = new LRUTest<>(4);
        for (int i = 0; i < 4; i++) {
            lru.put(String.valueOf(i),String.valueOf(i));
        }
//        lru.put("0" , "0");
//        lru.put("1" , "1");
//        lru.put("2" , "2");
        lru.get("0");
        lru.get("1");


        for (Map.Entry<String, String> stringStringEntry : lru.entrySet()) {
            System.out.printf("key:%s , value:%s \n" , stringStringEntry.getKey() , stringStringEntry.getValue());;
        }
//        lru.get("0");
//        lru.get("1");
//
        for (int i = 4; i < 6; i++) {
            lru.put(String.valueOf(i),String.valueOf(i));
        }
//        System.out.println(lru.size());
        for (Map.Entry<String, String> stringStringEntry : lru.entrySet()) {
            System.out.printf("key:%s , value:%s \n" , stringStringEntry.getKey() , stringStringEntry.getValue());;
        }
    }
}
