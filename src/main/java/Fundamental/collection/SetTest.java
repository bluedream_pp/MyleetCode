package Fundamental.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

public class SetTest {
    public static void main(String[] args) {
        HashSet<Book> set = new HashSet<>();
        set.add(new Book("ss"));
        set.add(new Book("dd"));
        Iterator<Book> iterator = set.iterator();
        for (Book book : set) {
            System.out.println(book);
            book.name = "ss";
        }
        for (Book book : set) {
            System.out.println(book);
        }
        System.out.println("----");
        set.add(new Book("ss"));
        for (Book book : set) {
            System.out.println(book);
        }
    }

    public static class Book {
        String name ;

        @Override
        public String toString() {
            return "Book{" +
                    "name='" + name + '\'' +
                    '}';
        }

        public Book(String name){
            this.name = name ;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Book book = (Book) o;

            return name.equals(book.name) ;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }
    }
}
