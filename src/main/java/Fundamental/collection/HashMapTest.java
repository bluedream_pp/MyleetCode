package Fundamental.collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashMapTest {
    public static void main(String[] args) {
        HashMap<String , Employee1> map = new HashMap<>();
        Employee1 e1 = new Employee1("james" , 23 , 100);
        map.put(e1.getName(), e1);
        Employee1 e2 = new Employee1("kobe" , 23 , 200);
        map.put(e2.getName(), e2);
        Employee1 e3 = new Employee1("kobe" , 23 , 900);
        map.put(e3.getName(), e3);

        Iterator<Map.Entry<String, Employee1>> iterator = map.entrySet().iterator();
        Map.Entry<String,Employee1> e = null ;
        System.out.println("---------entry set iterator");
        while (iterator.hasNext()){
            e = iterator.next();
            if(e.getValue().getSalary() > 200){
                System.out.println(e);
            }
        }

        System.out.println("---------key iterator");
        for (String s : map.keySet()) {
            if(map.get(s).getSalary() > 200){
                System.out.println(map.get(s));
            }
        }
    }
}
class Employee1 {
    private String name ;
    private int age ;
    private int salary ;

    @Override
    public String toString() {
        return "Employee1{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }

    public Employee1(String name, int age, int salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
