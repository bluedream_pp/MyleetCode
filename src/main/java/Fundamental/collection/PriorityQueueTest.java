package Fundamental.collection;

import java.util.*;

public class PriorityQueueTest {
    public static void main(String[] args) {
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1.equals(o2)){
                    return 0 ;
                }
                return (o2 - o1 > 0 ? 1 : -1) ;
            }
        };
        PriorityQueue<Integer> q = new PriorityQueue<>(comparator);
        q.add(1);
        q.add(2);
        System.out.println(q.remove());
        System.out.println(q.remove());

        PriorityQueue<Word> words  = new PriorityQueue<Word>();
        words.add(new Word('a' , 1));
        words.add(new Word('c' , 2));
        System.out.println(words.poll().c);
        TreeMap<Word , Integer> map = new TreeMap<>();
        map.put(new Word('a' , 3) , 1);
        map.put(new Word('b' , 2) , 1);
        System.out.println("------------");
    }
    public static class Word implements Comparable<Word>{
        public char c ;
        public int cnt ;

        @Override
        public String toString() {
            return "Word{" +
                    "c=" + c +
                    ", cnt=" + cnt +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Word word = (Word) o;
            return c == word.c && cnt == word.cnt;
        }

        @Override
        public int hashCode() {
            return Objects.hash(c, cnt);
        }

        public Word(char c , int cnt){
            this.c = c ;
            this.cnt = cnt ;
        }

        @Override
        public int compareTo(Word o2) {
            return  this.cnt -o2.cnt ;
        }
    }
}
