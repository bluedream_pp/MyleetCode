package Fundamental.collection;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class SortOnCollection {
    private List<Book> books ;

    public SortOnCollection(List<Book> books) {
        this.books = books;
    }
    public void setBooks(List<Book> books){
        this.books = books ;
    }

    public void sort(){
        if(null == books){
            System.out.println("book is empty");
            return;
        }
        for (int i = 0; i < books.size(); i++) {
            for (int j = i ; j < books.size() ; j++) {
                if(books.get(j).compareTo(books.get(i)) > 0){
                    swap(i , j);
                }
            }
        }
        for (Book book : books) {
            System.out.println(book);
        }
    }
    public void swap(int from , int to){
        Book swap = null ;
        swap = books.get(from);
        books.set(from, books.get(to));
        books.set(to , swap);
    }
    public static void main(String[] args) {
        ArrayList<Book> books = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            books.add(new Book("author" + i , Math.round(Math.random()*10) , "book" + i ));
        }
        SortOnCollection soc = new SortOnCollection(books);
        System.out.println("--------------使用 arraList -----------------");
        soc.sort();

        System.out.println("------------使用 LinkedList ------------");

        LinkedList<Book> books1 = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            books1.add(new Book("author" + i , Math.round(Math.random()*110) , "book" + i ));
        }
        soc.setBooks(books1);
        soc.sort();
        System.out.println("------------使用 Vector ------------");

        Vector<Book> books2 = new Vector<>();
        for (int i = 0; i < 10; i++) {
            books2.add(new Book("author" + i , Math.round(Math.random()*110) , "book" + i ));
        }
        soc.setBooks(books2);
        soc.sort();

    }
}
