package Fundamental.collection;


import java.util.HashSet;

/**
 * @className: HashSertConflinction
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/22 16:17
 */
public class HashSertConflinction {
    public static void main(String[] args) {
        HashSet hs = new HashSet();
        Book book = new Book("wyf", 1.0, "n");
        Book book1 = new Book("wyf11111", 1.0, "n11111");
        System.out.println(book.equals(book1));
        System.out.println(book.hashCode() == book1.hashCode());
        System.out.println(book.hashCode());
        hs.add(book);
        hs.add(book1);
        hs.forEach(x -> System.out.println(x));
    }
}
