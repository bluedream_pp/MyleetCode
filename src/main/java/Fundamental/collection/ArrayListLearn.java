package Fundamental.collection;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @className: ArrayListLearn
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/24 21:49
 */
public class ArrayListLearn {
    public static void main(String[] args) {
        List<String> fruits = new ArrayList<>();
        fruits.add("apple");
        fruits.add("orange");
        fruits.add("banner");

    }
}
