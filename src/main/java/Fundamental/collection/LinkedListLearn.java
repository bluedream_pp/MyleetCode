package Fundamental.collection;


import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @className: LinkedListLearn
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/26 7:47
 */
public class LinkedListLearn {
    public static void main(String[] args) {
        LinkedList<String> players = new LinkedList<>();
        players.add("Kobe");
        players.add("James");
        players.add("Atony");
        players.add(2 , "p1");
        players.addLast("p2");
        players.addFirst("P3");
        ArrayList<String> players2 = new ArrayList<>();
        players2.add("p4");
        players2.add("p5");
        players.addAll(players2);
        players.forEach(x-> System.out.println(x));
        System.out.println("------------");
        String remove = players.remove();
        System.out.println(remove);
    }
}
