package Fundamental.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorTest {
    public static void main(String[] args) {
        Collection<String> col = new ArrayList<>();
        col.add("a");
        col.add("b");
        Iterator<String> iterator = col.iterator();
        while (iterator.hasNext()) {
            String next =  iterator.next();
            System.out.println(next);
        }
        // 重置 iterator ，就是重新的弄一个 iterator 出来。之前的 iterator 就会被 GC 回收掉。
        iterator = col.iterator();
    }
}
