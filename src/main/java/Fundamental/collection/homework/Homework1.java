package Fundamental.collection.homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
/**
 * 作业要求：
 * 1. 封装一个新闻类，包含新闻标题和内容，提供 get set 方法，重写 toString 方法，打印对象的时候只打印标题。
 * 2. 只提供一个带参数的构造函数，构造函数中，只初始化标题，并且实例化两个对象：
 *    1） 比丢冠更寒心！韩德君老婆正式发声，愤怒回击辽迷：不要就让他走
 *    2） 中国男篮12人预测！辽宁2人，广东3人，多名小将有望杜锋重点培养
 * 3. 将这两个新闻添加到 ArrayList 中，并且进行倒序排序
 * 4. 在遍历新闻的过程中，对新闻标题进行处理，超过 15 个字的只保留前 10 字，然后后面使用 ... 替换
 * 5. 在控制太打印出处理后的标题。
 * */
public class Homework1 {
    public static void main(String[] args) {
        News news1 = new News("比丢冠更寒心！韩德君老婆正式发声，愤怒回击辽迷：不要就让他走");
        News news2 = new News("中国男篮12人预测！辽宁2人，广东3人，多名小将有望杜锋重点培养");
        ArrayList<News> list = new ArrayList<>();
        list.add(news1);
        list.add(news2);
        Collections.reverse(list);
        String head = null ;
        for (News news : list) {
            head = news.getHeader();
            if(head.length() > 10){
                System.out.println(head.substring(0 , 10) + "...");
            }
        }
    }
}
class News{
    private String header ;
    private String content ;

    public News(String header) {
        this.header = header;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        News news = (News) o;
        return (header.equals(news.header)) &&
                (content.equals(news.content));
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, content);
    }

    @Override
    public String toString() {
        return "News{" +
                "header='" + header + '\'' +
                '}';
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
