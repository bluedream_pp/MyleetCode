package Fundamental.collection.homework;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

/**
 * 说出下面代码的输出。
 *
 * */
public class Homework2 {
    public static void main(String[] args) {
        Person p = new Person(1 , "AA");
        Person p1 = new Person(3 , "BB");
        HashSet<Person> persons = new HashSet<>();
        persons.add(p);
        persons.add(p1);
        p.setName("CC");
        persons.remove(p);
        System.out.println("one :" + persons);

        persons.add(new Person(1,"CC"));
        System.out.println("two:" + persons);
        persons.add(new Person(4,"CC"));
        System.out.println("three:" + persons);
    }
}
class Person{
    private int id ;
    private String name ;

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return id == person.id &&
                Objects.equals(name, person.name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
