package Fundamental.collection;

import java.util.Objects;

public class Book implements Comparable<Book>{
    private String author ;
    private double price ;
    private String name ;

    public Book() {
    }

    public Book(String author, double price, String name) {
        this.author = author;
        this.price = price;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return book.getPrice() == this.getPrice()
                && book.getName().equals(this.getName())
                && book.getAuthor().equals(this.getAuthor())
                ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price);
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", price=" + price +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Book o) {
        if(this.price - o.getPrice() == 0){
            return 0 ;
        }
        return (this.price - o.getPrice() > 0 ? 1 : -1 );
    }
}
