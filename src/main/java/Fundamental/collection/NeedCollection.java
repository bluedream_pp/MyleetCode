package Fundamental.collection;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @className: NeedCollection
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/23 21:19
 */
public class NeedCollection {
    public static void main(String[] args) {
        // 1. 数组长度一旦定义，就不能改动了。
        // 2. 数组中的元素只能是相同类型。
        // 3. 数组中没有定义常见的算法和数据结构。
        Collection<String> list = new ArrayList();
        list.add("James");
        list.add("Kobe");
        list.add("Tom");
        list.add("aa");
        Spliterator<String> spliterator = list.spliterator();
//        Spliterator<String> stringSpliterator = spliterator.trySplit();
//        stringSpliterator.forEachRemaining(x-> System.out.println(x));
//        Spliterator<String> stringSpliterator1 = spliterator.trySplit();
//        stringSpliterator1.forEachRemaining(x-> System.out.println(x));

        Spliterator<String> stringSpliterator = null ;
        while(!Objects.isNull(stringSpliterator = spliterator.trySplit())){
            stringSpliterator.forEachRemaining(x-> System.out.println(x));
        }

//        list.stream().map((x) -> {
//            System.out.println(Thread.currentThread().getName());
//            System.out.println(x);
//            return x;
//        }).filter(new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return true;
//            }
//        });
//        System.out.println(collect);
//        Stream<String> stringStream = list.parallelStream();
//        stringStream.map((x)->{
//            System.out.println(Thread.currentThread().getName());
//            System.out.println(x);
//            return x;
//        });

    }
}
