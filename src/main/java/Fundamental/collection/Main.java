package Fundamental.collection;

import jdk.nashorn.internal.parser.JSONParser;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
//        Book b = new Book("asd" , 1 , "asd");
//        ArrayList<Book> books = new ArrayList();
//        books.add(b);
//        b.setAuthor("111");
//        Book book = books.get(0);
//        System.out.println(book == b);
//        System.out.println(b.getAuthor());
//
//        String author = b.getAuthor();
//        System.out.println(author == b.getAuthor());
//        double price = b.getPrice();
//        price = 1.21 ;
//        System.out.println(1.21 == b.getPrice());
        ArrayList<String> rs = new ArrayList<>();
//        Collections.sort();
        rs.add("x");
        rs.add("y");
        rs.add("y");
        rs.add("z");

////        rs.removeIf(x -> x.equals("x"));
//
        ArrayList<String> rs1 = new ArrayList<>();
        rs1.add("y");
        rs1.add("a");
        // 合
//        rs.addAll(rs1);
        // 做交集
//        rs.retainAll(rs1);
//
//        // 并
//        rs1.removeAll(rs);
//        rs.addAll(rs1);
        // 补
//        rs.removeAll(rs1);
//        rs.forEach(x -> System.out.println(x));
        Queue<String> q = new PriorityQueue<>();
        q.add("1");
        String[] strings = new String[q.size()];
        String[] strings1 = q.<String>toArray(strings);
        System.out.println(strings1 == strings);
        String[] ss = new String[]{"11aa", "011"};
        System.out.println(String.join(",", ss));
        Stream<String> sorted = Arrays.stream(ss).sorted();
        System.out.println(sorted.collect(Collectors.joining(",")));
    }
}
