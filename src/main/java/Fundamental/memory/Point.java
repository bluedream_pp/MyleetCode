package Fundamental.memory;
/**
 * Point class function description.
 *二维平面上的一个点，它有两个成员变量，X 代表了 x 轴的坐标，Y 代表 y 轴的坐标
*/
public class Point {
    private double X ;
    private double Y ;

    public Point(int x, int y) {
        this.X = x ;
        this.Y = y ;
    }

    public double getX() {
        return X;
    }

    public void setX(double x) {
        X = x;
    }

    public double getY() {
        return Y;
    }

    public void setY(double y) {
        Y = y;
    }
}
