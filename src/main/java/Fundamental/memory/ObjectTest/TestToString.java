package Fundamental.memory.ObjectTest;

public class TestToString {
    private int i = 1 ;

    @Override
    public String toString() {
        return String.valueOf(i);
    }

    public static void main(String[] args) {
        TestToString t = new TestToString();
        System.out.println("TestToString toString method output:" + t);
    }
}
