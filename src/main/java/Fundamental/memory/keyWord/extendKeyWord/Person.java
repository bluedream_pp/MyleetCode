package Fundamental.memory.keyWord.extendKeyWord;

public class Person {
    /**
     *age 是一个公共的属性，在这里不算作是公共的属性了。
     * */
    public int age ;
    /**
     * 一个人的名字有也是一个公共的属性。
     * */
    public String name ;
    /**
     * 一个人有多少个情人，这个是一个秘密，所以它是一个 private 的。
     * */
    private int loverCnt = 10;
    /**
     * 我多少钱，是不能告诉子女的，同一个目录下的类算作是家庭成员，也是可以知道的。
     * */
    double money = 100 ;
    /**
     *  个人的运动，家里面的人都知道，亲人知道，子女也知道。其他人不能知道了。
     * */
     protected String[] favorat = new String[]{"backetball" , "baseball" , "voleeball"} ;

    public Person(){}

    protected Person(int age , String name){
        this.age = age ;
        this.name = name ;
    }



    public void publicMethod(){}
    protected void protectedMethod(){}
    void defaultMethod(){}
    private void privateMethod(){}

    public int getLoverCnt(String pwd){
        // 口令对了，才能告诉有多少个情人
        if("twgdh".equals(pwd)){
            return loverCnt ;
        }
       return 0 ;
    }
}
