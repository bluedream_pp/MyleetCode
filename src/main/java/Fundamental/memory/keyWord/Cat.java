package Fundamental.memory.keyWord;

public class Cat {
    static public int sid ;
    public int id ;
    public String name ;

    public static void staticMethod(){
        // 静态成员里面只能访问静态变量和静态的方法
//        getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
