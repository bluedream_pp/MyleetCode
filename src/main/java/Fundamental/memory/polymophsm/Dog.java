package Fundamental.memory.polymophsm;

public class Dog extends Pet {
    private double speed ;
    public Dog(int age, int weight , double speed) {
        super(age, weight);
        this.speed = speed;
    }

    @Override
    public void show() {
        System.out.println("age:" + getAge()
            + ",weight:" + getWeight()+
                ",speed:" + speed
        );
    }
}
