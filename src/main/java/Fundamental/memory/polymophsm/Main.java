package Fundamental.memory.polymophsm;

public class Main {
    public static void main(String[] args) {
        Pet dog = new Dog(1,1,1.9);
        Pet fish = new Fish(1,2 , "red");
        dog.show();
        fish.show();
    }
}

