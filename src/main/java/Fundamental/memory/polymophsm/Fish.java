package Fundamental.memory.polymophsm;

public class Fish extends Pet {
    private String color ;
    public Fish(int age, int weight , String color) {
        super(age, weight);
        this.color = color ;
    }

    @Override
    public void show() {
        System.out.println("age: "+getAge()+" , weight:"+getWeight()+" , color:" + color);
    }
}
