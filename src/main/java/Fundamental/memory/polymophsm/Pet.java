package Fundamental.memory.polymophsm;

public class Pet {
    private int age ;
    private double weight;
    public Pet(int age , int weight){
        this.age = age ;
        this.weight =weight;
    }
    public void show(){
        System.out.println("age:"+age
         + "weight:" +weight
        );
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
