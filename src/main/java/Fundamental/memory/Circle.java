package Fundamental.memory;
/**
 * Circle 类的描述。
 * Circle 代表平面上的一个圆，它有两个属性，一个是 Point 一个是直径。
 * */
public class Circle {
    private Point p ;
    private double raduis ;

    public Circle(Point p, double raduis) {
        this.p = p;
        this.raduis = raduis;
    }

    public Point getP() {
        return p;
    }

    public void setP(Point p) {
        this.p = p;
    }

    public double getRaduis() {
        return raduis;
    }

    public void setRaduis(double raduis) {
        this.raduis = raduis;
    }
    /**
     * 计算某个点是否在圆里面
     * */
    public boolean containsPoint(Point p ){
        double r = Math.sqrt(Math.pow(this.p.getX() - p.getX() , 2) + Math.pow(this.p.getY() - p.getY() , 2));
        return (r > this.raduis?false:true);
    }
    /**
     * 计算圆的面积
     * */
    public double square(){
        return 3.14*Math.pow(this.raduis,2);
    }

    public static void main(String[] args) {
        Point p = new Point(0,0);
        Circle c = new Circle(p , 2);
        System.out.println(c.square());
        System.out.println(c.containsPoint(new Point(3,3)));
    }
}
