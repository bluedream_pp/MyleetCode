package Fundamental.memory.arrayTest;

public class Circle2 {
    private Kid head;
    private Kid tail;
    private int size ;
    public Circle2(int size){
        for(int i = 0 ; i < size ; i++){
            this.add(new Kid(i,null));
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Kid cur = head;
        int i = size ;
        while(i>0){
            sb.append(cur.getId() + ",");
            cur = cur.getNext();
            i--;
        }
        return sb.toString();
    }

    public int size(){
        return size ;
    }
    public void add(Kid kid){
        if(head == null){
            head = kid ;
            tail = kid ;
            head.setNext(kid);
        }else{
            tail.setNext(kid);
            kid.setNext(head);
            tail = kid ;
        }
        size++;
    }
    public void delete(Kid pre ,Kid kid){
        if(head == kid && tail == kid){
            return;
        }else if(head == kid){
           head = kid.getNext();
        }else if(tail == kid){
            tail = pre ;
        }
        pre.setNext(kid.getNext());
        kid = null ;
        size--;
    }
    public void count(){
        int j = 1;
        Kid pre = tail ;
        Kid cur = head ;
        Kid tempPre = null ;
        Kid tempCur = null ;
        while(size > 1){
            if(j >= 3){
                tempPre = pre ;
                tempCur = cur ;
                cur = cur.getNext();
                j = 1;
                delete(tempPre, tempCur);

            }else {
                pre = cur ;
                cur = cur.getNext();
                j++;
            }
            if(size == 1){
                System.out.println(head);
            }
        }
    }

    public static void main(String[] args) {
        Circle cc = new Circle(3, 1000);
        Circle2 c = new Circle2(1000);
        System.out.println("boolean:");
        cc.count2();
        System.out.println("link node");
        System.out.println(c.toString());
        c.count();
        System.out.println(c.toString());
    }
}
