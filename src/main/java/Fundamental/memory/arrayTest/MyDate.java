package Fundamental.memory.arrayTest;

public class MyDate {
    private int year ;
    private int month ;
    private int day ;

    @Override
    public boolean equals(Object date) {
        if( date instanceof MyDate){
            MyDate d = ((MyDate) date);
            return (this.day == d.getDay() && this.month == d.getMonth() && this.year == d.getYear() );
        }
        return false;
    }
    public int compare(MyDate d2){
        if(this.equals(d2)){
            return 0 ;
        }else if(this.getYear() > d2.getYear()
                || ( this.getYear() == d2.getYear() && this.getMonth() > d2.getMonth())
                || ( this.getYear() == d2.getYear() && this.getMonth() == d2.getMonth() && this.getDay() > d2.getDay())
        ){
            return 1 ;
        }else{
            return -1 ;
        }

    }

    @Override
    public String toString() {
        return "MyDate{" +
                "" + year +
                "-" + (month <10 ? "0" + month: month) +
                "-" + (day <10 ? "0" + day: day) +
                '}';
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
