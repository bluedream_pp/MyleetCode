package Fundamental.memory.arrayTest;

import Fundamental.sort.SelectSort;

import java.util.Arrays;

public class SortArgs {
    private int[] array = null ;
    public static void main(String[] args) {
        int[] a = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            a[i] = Integer.parseInt(args[i]);
        }
        System.out.println("original order list:");
        System.out.println(Arrays.toString(a));
        SortArgs sa = new SortArgs();
        sa.setArray(a);
        sa.sort();
        System.out.println("ordered list:");
        System.out.println(Arrays.toString(a));
    }

    private void sort() {
        SelectSort ss = new SelectSort(array);
        ss.sort();
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}
