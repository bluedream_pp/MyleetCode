package Fundamental.memory.arrayTest;

public class Kid {
    private int id ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kid getNext() {
        return next;
    }

    public void setNext(Kid next) {
        this.next = next;
    }

    private Kid next;

    public Kid(int id, Kid next) {
        this.id = id;
        this.next = next;
    }

    @Override
    public String toString() {
        return "Kid{" +
                "id=" + id +
                '}';
    }
}
