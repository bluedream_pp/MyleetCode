package Fundamental.memory;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * @className: VolatileTest
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/6/28 8:33
 */
public class VolatileTestBoolean {
    public static volatile boolean running = true ;

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(running){
                    System.out.println("runing");
                    try {
                        TimeUnit.SECONDS.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("stop");
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(6);
                    running = false ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
}
