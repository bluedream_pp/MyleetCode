package Fundamental.memory;


public class ConstractMethod {
    public int x ;
    public int y ;
    /*
    *不指定构造函数，会给一个下面一样的无参的构造函数。
    * */
    ConstractMethod(){

    }
    /*
    * 写一个有参数的构造函数。
    * */
    ConstractMethod(int _x , int _y ){
       this.x = _x ;
        this.y = _y ;
    }
    /*
    *
    * 如果在构造函数的前面加上了返回值，如下面的函数就不是构造函数了。它是一个普通的函数。
    * */
    void ConstractMethod(int _x , int _y ){
        this.x = _x ;
        this.y = _y ;
    }
    public static void main(String[] args) {
        // 使用有参数的构造函数。
        ConstractMethod c = new ConstractMethod(1,2);
        c.ConstractMethod(1,2);


    }
}
