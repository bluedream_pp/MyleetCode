package Fundamental.enumration;

public enum WeekDay {
    /**
     * 表示星期一
     * */
    Mon( 1, "Monday"),
    Tue( 2, "Tuesday"),
    Wed( 3, "Wednesday"),
    Thu( 4, "Thursday"),
    Fri( 5, "Friday"),
    Sun( 1, "Sunday"),
    ;

    private int index ;
    private String day;
    WeekDay(int index, String day) {
        this.index = index ;
        this.day = day ;
    }
    public void printDay(int index ){
        switch (index){
            case 1 :
                System.out.println(WeekDay.Mon);
                break;
           case 2 :
               System.out.println(WeekDay.Tue);
               break;
            default:
               System.out.println("invalidate index" + index);
        }
    }

    @Override
    public String toString() {
        return "WeekDay{" +
                "index=" + index +
                ",day='" + day + '\'' +
                ",order='" + this.ordinal()+ '\'' +
                '}';
    }

    public static void main(String[] args) {
        WeekDay wd = WeekDay.Fri;
        System.out.println(wd.toString());
        wd.printDay(1);
    }
}
