package Fundamental.enumration;

public class Main {
    public static void main(String[] args) {
        Color c = Color.BLUE;
        Color color = Color.valueOf("RED");
        System.out.println(color);
        for (Color value : Color.values()) {
            System.out.println(value);
        }
    }
}
