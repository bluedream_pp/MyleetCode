package Fundamental.enumration;

import java.util.EnumMap;

public class DBBaseType {
    private EnumMap<DBTYPE,String> urls = new EnumMap<DBTYPE, String>(DBTYPE.class);
    public String getUrl(DBTYPE type){
        return urls.get(type);
    }
    DBBaseType(){
        urls.put(DBTYPE.MYSQL,"mysql_url_template");
        urls.put(DBTYPE.DB2,"db2_url_template");
        urls.put(DBTYPE.SQLSERVER,"sqlserver_url_template");
    }

    public static void main(String[] args) {
        DBBaseType dbt = new DBBaseType();
        System.out.println(dbt.getUrl(DBTYPE.DB2));
    }
}
