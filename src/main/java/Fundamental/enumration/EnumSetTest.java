package Fundamental.enumration;

import java.util.EnumMap;
import java.util.EnumSet;

public class EnumSetTest {
    public static void main(String[] args) {
        // 取出所有的
        EnumSet<DBTYPE> es = EnumSet.allOf(DBTYPE.class);
        for (DBTYPE e : es) {
            System.out.println(e);
        }
    }
}
