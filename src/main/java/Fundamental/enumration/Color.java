package Fundamental.enumration;

public enum Color {
    /**
     * 红色
     * */
    RED,
    /**
     * 蓝色
     * */
    BLUE
}
