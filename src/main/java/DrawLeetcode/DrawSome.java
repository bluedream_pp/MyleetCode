package DrawLeetcode;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class DrawSome {
    private static final int FRAME_WIDTH = 500 ;
    private static final  int FRAME_HEIGHT = 800 ;
    private static final  int RECT_WIDTH = 800 ;
    private static final  int RECT_HEIGHT = 800 ;
    public static void main(String[] args) {
        Frame f = new Frame();
        f.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        f.setResizable(true);
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

//        Image image = f.createImage(FRAME_WIDTH, FRAME_HEIGHT);
//        Graphics graphics = image.getGraphics();
//        graphics.setColor(Color.BLACK);
//        graphics.fillRect( 0 ,0  , RECT_WIDTH , RECT_HEIGHT);
//        .drawImage(image , 0 , 0 , null);
        MyJPanel panel = new MyJPanel();
        f.add(panel);
        f.setVisible(true);

    }
}
