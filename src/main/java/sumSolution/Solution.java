package sumSolution;
/**
* 求1+2+3+...+n，要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A?B:C）。
* */
public class Solution {
    public int sumSolution(int i){
        int n = i ;
        boolean b = i > 1 && (n +=  sumSolution(i - 1))>0;
        return n ;
    }
    public static void main(String[] args) {
        System.out.println(new Solution().sumSolution(100));
    }
}
