package shortestIncludeStr;

import java.util.Arrays;

// 1. window 里面，第一个字符必须在 big 里面
// 2. 还要用一个变量来记录 window 里面字符的个数
// 3. 等到了 little 的个数之后，把 window 里面的东西清空，清空后继续之前的程序。
public class Solution {
    public void method(char[] big , char[] little){
        char[] window = new char[100];
        int windowIdx = 0 ;
        int includeNum = 0 ;
        int numLittle = little.length;
        char[] rs = null ;
        for(char e : big){
            if(includeNum > 0){

                // little 里面的字符都在 window 里面了，打印出来，清楚 window
                if(includeNum == numLittle){
                    if(null == rs || rs.length > windowIdx+1){
                        rs = Arrays.copyOf(window,window.length);
                    }
                    windowIdx = 0 ;
                    includeNum = 0 ;
                    setZero(window);
                    if(includeInd(little,e)){
                        window[windowIdx++] = e ;
                        includeNum++;
                    }
                }else{
                    window[windowIdx++] = e ;
                    if(includeInd(little , e)){
                        includeNum++ ;
                    }
                }
            }
            // window 里面包含 little 中的数目还没有全
            else if(includeNum == 0 && includeInd(little , e)) {
                // window 里面的没有字符的情况下，只能往里面放 little 里面的字符。
                    window[windowIdx++] = e ;
                    includeNum++;
                }
        }

        if( includeNum > 0 && includeNum == numLittle){
            if(null == rs || rs.length > windowIdx+1){
                rs = Arrays.copyOf(window,window.length);
            }
        }
        System.out.println(rs);
    }
    private void setZero(char[] a ){
        for(int i = 0 ; i < a.length;i++){
            a[i] = 0 ;
        }
    }
    private boolean includeInd(char[] little , char target){
        int idx = 0 ;
        for(; idx < little.length ; idx++){
            if(little[idx] == target){
                return true;
            }
        }
        return false ;
    }
}
