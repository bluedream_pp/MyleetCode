package shortestIncludeStr;

public class Main {
    public static void main(String[] args) {
        Solution s = new Solution();
//        s.method("abcde".toCharArray(),"a".toCharArray());
//        s.method("abcde".toCharArray(),"ab".toCharArray());
//        s.method("abcde".toCharArray(),"ac".toCharArray());
//        s.method("abcde".toCharArray(),"ae".toCharArray());
//
        s.method("abcde".toCharArray(),"abd".toCharArray());
        s.method("abcde".toCharArray(),"ace".toCharArray());
        s.method("abcdeabcac".toCharArray(),"ac".toCharArray());
    }
}
