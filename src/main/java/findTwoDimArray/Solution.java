package findTwoDimArray;
/* 1 2 8  9
*  2 4 9  12
*  4 7 10 13
*  6 8 11 15
*/
public class Solution {
    public boolean find(int target , int[][] ary){
        int rs = 0 ;
        int middleX = ary[0].length - 1;
        int middleY = 0 ;

        while(middleX > 0 && middleY < ary.length){
            // 如果大于，则 middleY 加一
            if(ary[middleX][middleY] < target){
                 middleY++;
                // 如果小于，则 middleY 减一
            }else if(ary[middleX][middleY] > target){
                middleX--;
                // 如果等于，则返回位置
            }else if(ary[middleX][middleY] == target) {
                return true;
            }
        }
        return false;
    }
}
