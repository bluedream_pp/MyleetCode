package TwoNumberSum;

import static java.util.Arrays.sort;

public class Main {
    public static void main(String[] args) {
        int[] ints = { 7 , 2 , 14 , 15 , 16};
        int target = 18 ;
        sort(ints);
        solution(ints, target);
    }

    private static int solution(int[] ints, int target) {
        StringBuilder sb = new StringBuilder();
        for (int anInt : ints) {
           sb.append(anInt + ",");
        }
        System.out.println("sorted array is :" + sb.toString());
        int head = 0 ;
        int tail = ints.length -1 ;
        while( head<tail && ints[head]+ints[tail] != target){
            if(ints[head]+ints[tail] > target){
                tail--;
            }else if(ints[head]+ints[tail] < target){
                head++;
            }
        }
        if(head == tail ){
            System.out.println("not find the target number");
        }else {
            System.out.println("head is: " + head + ", tail is :" + tail);
        }
        return 0;
    }
}
