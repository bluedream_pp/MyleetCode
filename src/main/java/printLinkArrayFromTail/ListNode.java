package printLinkArrayFromTail;

public class ListNode {
        private int val ;
        private ListNode next ;

        ListNode(int val){
            this.val = val ;
        }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
            this.next = next;
    }

    public Integer getVal() {
            return this.val;
    }
}
