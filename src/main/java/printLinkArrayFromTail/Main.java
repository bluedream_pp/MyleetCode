package printLinkArrayFromTail;


import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(4);
        ListNode l5 = new ListNode(5);
        l1.setNext(l2);
        l2.setNext(l3);
        l3.setNext(l4);
        l4.setNext(l5);
        ArrayList<Integer> list = new ArrayList<>();
        new Solution().printListFromTailToHead(l1,list);
        for(Integer i : list){
            System.out.println(i);
        }
    }
}
