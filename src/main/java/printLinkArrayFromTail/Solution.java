package printLinkArrayFromTail;

import java.util.ArrayList;

public class Solution {
    public ArrayList<Integer> printListFromTailToHead(ListNode head , ArrayList<Integer> list){
        if(head != null){
            printListFromTailToHead(head.getNext(),list);
            list.add(head.getVal());
        }
        return list ;
    }
}
