package firstNotRepeatingChar;

import java.util.BitSet;

public class Solution {
    public int firstNotRepeatingChar(String in ){
        int rs = -1 ;
        if(in.length() > 10000){
            return rs ;
        }
        BitSet bitSet1 = new BitSet();
        BitSet bitSet2 = new BitSet();
        for(char e : in.toCharArray()){
            if(!bitSet1.get(e)){
                bitSet1.set(e);
            }else{
               bitSet2.set(e);
            }
        }
        char[] chars = in.toCharArray();
        for(int e = 0 ; e < chars.length;e++ ){
            if(bitSet1.get(chars[e]) && !bitSet2.get(chars[e])){
                rs = e ;
                break;
            }
        }
        return rs ;
    }
    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println(s.firstNotRepeatingChar("aaaecaaab"));
    }
}
