package findBinTreeNextNode;

public class TreeNode {
    private int var ;
    private TreeNode left ;
    private TreeNode right ;
    private TreeNode father;

    @Override
    public String toString() {
        return "TreeNode{" +
                "var=" + var +
                '}';
    }

    public TreeNode(int var) {
        this.var = var;
    }

    public int getVar() {
        return var;
    }

    public void setVar(int var) {
        this.var = var;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }

    public TreeNode getFather() {
        return father;
    }

    public void setFather(TreeNode father) {
        this.father = father;
    }
}
