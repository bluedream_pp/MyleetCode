package JVM;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @className: StringTableTune3
 * @Description:
 *
 * @Author: wangyifei
 * @Date: 2022/7/28 18:56
 */
public class StringTableTune3 {
    public static void main(String[] args) throws IOException {
     String[] strs = {
            "elebfev",
            "kchozpvnniwgzsrqbiyiqmpgj",
            "kkhyihiddgttvxwcskny",
            "qe",
            "kfixw",
            "brufik",
            "zrjfqmkwcnjcxqlhihheqi",
            "acrlpweendmfjjaqgv",
            "pmqwkisddjuums",
            "pklxzddu",
            "nyazpmzfbksvt",
            "tuupjdmdal",
            "upctapcqthmsjzpgdw",
            "gss",
     };
        Random r = new Random();
        List<String> list = new ArrayList<>();
        int j = 0 ;
        try{
            String s = null ;
            for (; j <10000000; j++) {
                s = strs[r.nextInt(strs.length-1)] + "";
//                list.add(s.intern());
                list.add(s);
            }
        }catch (Throwable t){
            t.printStackTrace();
        }finally {
            System.out.println(j);
        }
        System.gc();
        System.in.read();
    }
}
