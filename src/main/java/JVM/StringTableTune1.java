package JVM;


/**
 * @className: StringTableTune
 * @Description: 证明 StringTable 是由 GC 的
 * @Author: wangyifei
 * @Date: 2022/7/28 18:56
 */
public class StringTableTune1 {
    public static void main(String[] args) {
        int j = 0 ;
        try{
            for (; j <10000; j++) {
                String.valueOf(j).intern();
            }
        }catch (Throwable t){
            t.printStackTrace();
        }finally {
            System.out.println(j);
        }
    }
}
