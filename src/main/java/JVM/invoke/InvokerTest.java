package JVM.invoke;


import java.util.ArrayList;
import java.util.List;

/**
 * @className: InvokerTest
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/12 21:24
 */
public class InvokerTest {
    public static void staticMethod(){
        System.out.println("static method");
    }
    public void virtualMethod(){
        System.out.println("virtual method");
    }
    public static void main(String[] args) {
        // invokervirotual
        new InvokerTest().virtualMethod();
        // invokerspecial

        // invokerstatic
        InvokerTest.staticMethod();
        List<String> list = new ArrayList<>();
        // invokerinterface
        list.size();
        ArrayList<String> list1 = new ArrayList<>();
        // invokervirtual
        list1.size();
        // invokerDynamic
        Runnable r = () ->{
            System.out.println("haha");
        };
        I i = C::n;
        II ii = C::y;
    }
    @FunctionalInterface
    public interface II {
        void m1(int x , int y);
    }

    @FunctionalInterface
    public interface I {
        void m1();
    }
    public static class C{
        public static void y(int i , int y){
            System.out.println("aa");
        }
        public static void n(){
            System.out.println("oo");
        }
    }
}
