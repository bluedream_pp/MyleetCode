package JVM;

public class ShowLayout {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long l = runtime.totalMemory();
        long maxMemory = runtime.maxMemory();
        long freeMemory = runtime.freeMemory();
        System.out.println("the initial heap size is" + l + " bytes , "+ l/(1024*1024)+"m");
        System.out.println("the max heap size is" + maxMemory + " bytes , "+ maxMemory/(1024*1024)+"m");
        System.out.println("the free heap size is" + freeMemory + " bytes , "+ freeMemory/(1024*1024)+"m");
        String a = "a";
        try {
            while (true){
                a += "sdfasdfjdsfwefawefawsefasdfseasef";
                Thread.sleep(1000);
                runtime.gc();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
