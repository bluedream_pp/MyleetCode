package JVM;


/**
 * @className: StringTableTune
 * @Description: StringTable 是一个 HashTable 的数据结构，所以如果增大
 *               里面数组的长度，则可减少 hash 碰撞，减小链表的长度
 * @Author: wangyifei
 * @Date: 2022/7/28 18:56
 */
public class StringTableTune2 {
    public static void main(String[] args) {

        int j = 0 ;
        try{
            String s = null ;
            for (; j <1000000; j++) {
                s = StringRandomTool.randomString();
                s.intern();
            }
        }catch (Throwable t){
            t.printStackTrace();
        }finally {
            System.out.println(j);
        }
    }
}
