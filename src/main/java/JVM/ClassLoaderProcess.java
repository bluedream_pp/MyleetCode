package JVM;

import java.io.IOException;

public class ClassLoaderProcess {
    public static void main(String[] args) throws IOException {
        // 父加载器，不是类加载器的加载器，也不是类加载器的父类加载器


        // 这是 类的加载器
        System.out.println(ClassLoaderProcess.class.getClassLoader());
        // 这是类加载器的加载器， 是 Bootstrap 加载器
        System.out.println(ClassLoaderProcess.class.getClassLoader().getClass().getClassLoader());
        // 这是类加载器的父加载器的父加载器
        System.out.println(ClassLoaderProcess.class.getClassLoader().getParent().getParent());

    }
}
