package JVM.classFileFormat;

/**
 * @className: ConstructorWithArgsClass
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/6 22:02
 */
public class ConstructorWithArgsClass {
    public ConstructorWithArgsClass(String a1 , int[] a2 , Integer a3){
        a1 = "aa";
        a2[1] = 1 ;
    }
}
