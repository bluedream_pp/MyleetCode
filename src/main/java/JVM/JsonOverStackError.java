package JVM;


import java.util.Arrays;
import java.util.List;

/**
 * @className: JsonOverStackError
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/26 21:34
 */
public class JsonOverStackError {
    public static void main(String[] args) {
        JsonOverStackError error = new JsonOverStackError();
        JsonOverStackError.Dept d = error.new Dept();
        d.setName("Sellar");

        Employee employee = error.new Employee();
        JsonOverStackError.Employee e1 = error.new Employee();
        employee.setName("Tom");
        e1.setName("J");
        d.setEmployee(Arrays.asList(employee , e1));
    }

    public class Employee{
        private String name ;
        private Dept dept ;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Dept getDept() {
            return dept;
        }

        public void setDept(Dept dept) {
            this.dept = dept;
        }
    }
    public class Dept{
        private String name ;
        private List<Employee> Employee ;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<JsonOverStackError.Employee> getEmployee() {
            return Employee;
        }

        public void setEmployee(List<JsonOverStackError.Employee> employee) {
            Employee = employee;
        }
    }
}
