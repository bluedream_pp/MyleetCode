package JVM;


/**
 * @className: TestIPlusPlus
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/10 22:05
 */
public class TestIPlusPlus {
    public static void main(String[] args) {

        int a = 8 ;
        a = a++ ;
        System.out.println(a);
        int i = 8 ;
        i = ++i  ;
        System.out.println(i);
//        m(Integer.valueOf(1),1);
    }
    public static void m(int a , int b){
        System.out.println("int");
        int c = a + b ;
    }
    public static void m(Integer a , int b){
        System.out.println("Integer");
        Integer c = a + b ;
    }
}
