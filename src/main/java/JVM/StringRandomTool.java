package JVM;


import java.util.Random;

/**
 * @className: StringRandomTool
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/28 19:27
 */
public class StringRandomTool {
    public static String randomString(){
        String rs = null ;
        Random r = new Random();
        int cnt = r.nextInt(26);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cnt; i++) {
            sb.append((char) ('a' + r.nextInt(26)));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            System.out.println(randomString());
        }
    }
}
