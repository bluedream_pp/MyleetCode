package JVM;


/**
 * @className: ObjectNewCircle
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/28 20:29
 */
public class ObjectNewCircle {
    public static int a = 5 ;
    public int b ;
    static {
        System.out.println("static");
        a += 6 ;
    }
    public ObjectNewCircle(){
        a += 1 ;
        System.out.println("constructor");
    }
    {
        System.out.println("constructor code block");
    }

    public static void main(String[] args) {
        ObjectNewCircle o = new ObjectNewCircle();
    }
}
