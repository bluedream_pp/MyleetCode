package JVM;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OOM1 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (int i = 0; true ; ) {
            executorService.submit(()->{
                System.out.println("timeout");
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
