package JVM;

/**
 * @className: JmapUsage
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/7/27 7:57
 */
public class JmapUsage {
    public static void main(String[] args) throws InterruptedException {
        System.out.println(1);
        Thread.sleep(30000);
        // 10M
        byte[] bs = new byte[1024*1024*10];

        System.out.println(2);
        Thread.sleep(30000);

        bs = null ;
        System.gc();
        System.out.println(3);
        Thread.sleep(300000);

    }
}
