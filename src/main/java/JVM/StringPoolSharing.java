package JVM;


/**
 * @className: StringPoolSharing
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/27 13:57
 */
public class StringPoolSharing {
    public static class T {
        public String a = "aa" ;
    }
    public static class T1 {
        public String a = "aa" ;
    }
    public static void main(String[] args) {
        System.out.println(new T().a == new T1().a);
    }
}
