package JVM;


/**
 * @className: Initialization
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/8 7:31
 */
public class Initialization {
    public static void main(String[] args) {
        System.out.println(T.i); // 3
    }
    public static class T{
        public static T t = new T();
        public static int i = 2 ;
        public T(){
            i++;
        }
    }
}
