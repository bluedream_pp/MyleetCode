package processBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @className: Test
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/25 10:37
 */
public class Test {
    private static Logger logger = LoggerFactory.getLogger(Test.class);

    public static void main(String[] args) throws Exception{
        String command = "ssh";
        String[] arguments = {command , "user@remote_host", "sh", "/path/to/script.sh"};
        ProcessBuilder processBuilder = new ProcessBuilder(arguments);
        Process process = processBuilder.start();
// 读取命令输出
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
// 等待命令执行完毕
        int exitCode = process.waitFor();
        System.out.println("Command exit code: " + exitCode);


    }
}
