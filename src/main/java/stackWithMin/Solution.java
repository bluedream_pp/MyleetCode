package stackWithMin;

public class Solution {
    private int[] data = new int[100];
    private int idx = 0 ;
    private Integer min = null ;
    public void push(int i ){
        if( min == null || i < min ){
            min = i ;
        }
        data[idx++] = i ;
    }
    public int pop(){
        if(--idx <= 0 ){
            return -1 ;
        }
        return data[idx] ;
    }
    public int top(){
        return 0 ;
    }
    public int min(){
        return min ;
    }
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.push(1);
        solution.push(2);
        solution.push(3);
        solution.push(4);
        solution.push(5);
        solution.push(6);
        solution.push(7);
        solution.push(-7);
        System.out.println(solution.min());
    }

}
