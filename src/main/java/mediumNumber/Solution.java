import java.util.Scanner;

/**
 * @desc:题目描述
 * 如何得到一个数据流中的中位数？如果从数据流中读出奇数个数值，那么中位数就是所有数值排序之后位于中间的数值。
 * 如果从数据流中读出偶数个数值，那么中位数就是所有数值排序之后中间两个数的平均值。
 * 我们使用Insert()方法读取数据流，使用GetMedian()方法获取当前读取数据的中位数。
 *
 * 保存数字的数字为 A ，长度为 n
 *
 * 我思路是：
 * 1. 如果 A 的字符个数为偶数，则使用插入排序找到第 n/2 个元素，然后返回 (A[n/2] + A[n/2-1] )/2
 * 2. 如果 A 的字符个数为奇数，则使用插入排序找到第 n/2 个元素，然后返回 A[n/2]
 *
 * 还有另外一种解法，是利用堆的性质，我还不知道堆的那个性质。大根堆和小根堆。
 *
 * @author wangyifei
 */
public class Solution {
    String inputStr = "" ;
    public void insert(){
        Scanner s = new Scanner(System.in);
        this.inputStr = s.nextLine() ;
    }
    public void swap(char[] array , int start , int end ){
        char swap = array[start];
        array[start] = array[end];
        array[end] = swap ;
    }
    public double find(char[] chs , int ord ){
        boolean isOld = (chs.length%2 == 1);
        int min = 0;
        int j = 0 ;
        for(int i = 0 ; i <= ord ; i++){
             j = i ;
             min = i ;
            for(; j < chs.length; j++){
                if(chs[j] < chs[min] ){
                    min = j;
                }
            }
            swap(chs,i ,min);
        }
        if(isOld){
           return new Double(chs[ord] - '0' );
        }

        return new Double(chs[ord] -'0' + chs[ord-1] - '0')/2;

    }
    public double getMedian(){
        char[] chs = inputStr.toCharArray();
        int start = 0 ;
        return find(chs,chs.length/2) ;
    }
    public static void main(String[] args) {
        Solution s = new Solution();
        s.insert();
        System.out.println(s.getMedian());

    }
}
