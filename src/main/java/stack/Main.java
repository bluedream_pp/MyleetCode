package stack;


import java.util.Scanner;

/**
 * @className: Main
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/12 17:20
 */
public class Main {
    private int[] data ;
    private int top_pointer = 0 ;
    private int size ;
    public Main(int capacity){
        data = new int[capacity];
        size = 0 ;
    }
    public void push(int i){
        data[top_pointer++] = i ;
    }
    public int pop(){
        if( size == 0 ){
            System.out.println("error");
            return -1 ;
        }
        return data[--top_pointer];
    }
    public int top(){
        if(size == 0){
            System.out.println("error");
            return -1;
        }
        return data[top_pointer - 1];
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String next = scanner.next();
        Main m = new Main(Integer.parseInt(next));
        String cmd = "" ;
        String ags = "" ;
        String[] s = null ;
        while (scanner.hasNextLine()){
            next = scanner.next();
            s = next.split(" ");
            cmd = s[0];
            if(cmd.equals("push")){
                m.push(Integer.parseInt(s[1]));
            }else if(cmd.equals("pop")){
                System.out.println(m.pop());
            }else if(cmd.equals("top")){
                System.out.println(m.top());
            }
        }
    }
}
