package deleteDuplicateNode;

public class Solution {
    public ListNode deleteDuplication(ListNode listNode){
        if(null == listNode){
            System.out.println("list node is invalidate");
        }
        if(null == listNode.getNext()){
            return listNode;
        }
        ListNode preNode = listNode ;
        ListNode next = listNode.getNext();
        do{
            if( preNode.getKey() == next.getKey()){
                preNode = listNode;
                preNode.setNext(next.getNext());
            }else{
                preNode = next;
            }
        }while ((next = next.getNext()) != null );

        return listNode;
    }
    public void forNodes(ListNode head){
        ListNode next = null ;
        next = head ;
        do{
            System.out.println(next.getKey());
        }while((next = next.getNext()) != null );

    }
}
