package deleteDuplicateNode;
//在一个排序的链表中，存在重复的结点，请删除该链表中重复的结点，重复的结点不保留，返回链表头指针。 例如，链表1->2->3->3->4->4->5 处理后为 1->2->5
public class Main {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1,null);
        ListNode l2 = new ListNode(2,null);
        ListNode l3 = new ListNode(3,null);
        ListNode l4 = new ListNode(4,null);


        l1.setNext(l2);
        l2.setNext(l3);
//        l3.setNext(l4);
        Solution solution = new Solution();
        ListNode head = solution.deleteDuplication(l1);
        solution.forNodes(head);
    }
}
