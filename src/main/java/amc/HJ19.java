package amc;

import java.util.*;
/**
 * @className: HJ19
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/3 18:01
 */
public class HJ19 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        Count cnt = new Count();
        String[] lines = {
            "E:\\bjo\\fwd\\qzc\\kptakwrxjpbqzjs 648"
           ,"F:\\cda\\tiqbfpjyfvybnbpbwwkwznidrmtjon 628"
           ,"G:\\gozugd\\tiqbfpjyfvybnbpbwwkwznidrmtjon 628"
           ,"D:\\zozfuu\\vne\\fdfnb 639"
           ,"G:\\pap\\tiqbfpjyfvybnbpbwwkwznidrmtjon 629"
           ,"D:\\mwtfprykzpfrcwf 632"
           ,"E:\\zcslks\\fdfnb 641"
           ,"E:\\bq\\qwhmi 650"
           ,"G:\\ehon\\spoojj 632"
           ,"D:\\cly\\fvur\\kw\\abkvychmbd 649"
           ,"E:\\dwm\\kptakwrxjpbqzjs 629"
           ,"F:\\jphr\\vu\\xzkpmhsammtwet 635"
           ,"F:\\jz\\nzee\\gil\\qwhmi 643"
           ,"E:\\guo\\kptakwrxjpbqzjs 646"
           ,"F:\\fdfnb 631"
           ,"C:\\ralrf\\dfxixc\\lraiancsfqtn 647"
           ,"C:\\sphml\\znidrmtjon 631"
           ,"F:\\lw\\akiom\\tiqbfpjyfvybnbpbwwkwznidrmtjon 635"
           ,"G:\\znidrmtjon 641"
           ,"D:\\xc\\yv\\mfvoiq\\lraiancsfqtn 645"
           ,"C:\\uimka\\xevqf\\qwhmi 639"
           ,"G:\\tijlq\\mwtfprykzpfrcwf 631"
           ,"G:\\tppz\\teym\\uehxhx\\mwtfprykzpfrcwf 639"
           ,"G:\\fdfnb 631"
           ,"E:\\ru\\tlql\\spoojj 633"
           ,"G:\\fdfnb 630"
           ,"C:\\otf\\xkl\\neqzv\\tiqbfpjyfvybnbpbwwkwznidrmtjon 644"
           ,"G:\\kb\\nezca\\whh\\xzkpmhsammtwet 642"
           ,"E:\\lwtk\\yqvd\\fdfnb 633"
           ,"D:\\lraiancsfqtn 642"
           ,"C:\\dn\\ymmfc\\znidrmtjon 646"
           ,"F:\\eckkq\\dnpy\\mwtfprykzpfrcwf 637"
           ,"E:\\jf 629"
           ,"G:\\abkvychmbd 645"
           ,"C:\\zngatv\\znidrmtjon 648"
           ,"E:\\bf\\webfjn\\qwai\\spoojj 638"
           ,"F:\\abkvychmbd 645"
           ,"E:\\cp\\fqus\\yrv\\spoojj 629"
           ,"D:\\ddmf\\fdfnb 648"
           ,"E:\\tiilpw\\hv\\iyx\\kptakwrxjpbqzjs 637"
           ,"D:\\lraiancsfqtn 631"
           ,"G:\\kptakwrxjpbqzjs 629"
           ,"D:\\bnmx\\eae\\cjcvj\\spoojj 635"
        };
//        while (in.hasNextLine()) { // 注意 while 处理多个 case
        for(String line :lines){
//            String line = in.nextLine();
            if("end".equals(line)){
                break ;
            }
            cnt.count(line);
        }
        for(FileInfo fi : cnt.dq){
            System.out.println(fi.fileName + " " + fi.line + " " + fi.count);
        }
    }
    public static class Count{
        public Deque<FileInfo> dq = new ArrayDeque();
        public HashSet<FileInfo> set = new HashSet();
        public void count(String line){
            String[] words = line.split("\\s+");
            String fileName = words[0].substring(words[0].lastIndexOf("\\") + 1);
            String l = words[1];
            FileInfo fi = new FileInfo(fileName.substring(fileName.length() - 16 >= 0 ? fileName.length() - 16: 0) , l);
            FileInfo last = dq.peekLast();
            if(fi.equals(last)){
                dq.peekLast().count += 1;
            }else{
                if(dq.size()>=8 && !set.contains(fi)){
                    dq.pollFirst();
                    dq.addLast(fi);
                    set.add(fi);
                }else{
                    if(!set.contains(fi)) dq.addLast(fi);
                    set.add(fi);
                }

            }

        }

    }
    public static class FileInfo{
        public String fileName ;
        public String line ;
        public int count ;
        public int actualCnt ;
        public FileInfo(String fileName , String line){
            this.fileName = fileName;
            this.line = line ;
            count ++;

        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FileInfo fileInfo = (FileInfo) o;
            return count == fileInfo.count && actualCnt == fileInfo.actualCnt && fileName.equals(fileInfo.fileName) && line.equals(fileInfo.line);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fileName, line);
        }
    }
}
