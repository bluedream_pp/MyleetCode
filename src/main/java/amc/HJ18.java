package amc;
import java.util.*;

/**
 * @className: HJ18
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/3 13:33
 */

public class HJ18 {
    public static final int A_IP = 0 ;
    public static final int B_IP = 1 ;
    public static final int C_IP = 2 ;
    public static final int D_IP = 3 ;
    public static final int E_IP = 4 ;

    public static final int ERROR_IP = 5 ;
    public static final int ERROR_MASK = 6 ;
    public static final int PRIVATE_IP = 7 ;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] result = new int[8];
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            String[] split1 = line.split("~");
            if("end".equals(split1[0])){
                break;
            }
            String[] ip = split1[0].split("\\.");
            String[] mask = split1[1].split("\\.");
            // 判断无效地址
            boolean flag = true ;
            if(ip.length < 4
                    || ip[0].length() == 0
                    || ip[1].length() == 0
                    || ip[2].length() == 0
                    || ip[3].length() == 0
            ){
                result[ERROR_IP]++;
                flag = false ;
            }


            int p1 = Integer.parseInt(mask[0]);
            int p2 = Integer.parseInt(mask[1]);
            int p3 = Integer.parseInt(mask[2]);
            int p4 = Integer.parseInt(mask[3]);
            // 判断掩码
            int all = 0 ;
            all += (p1<<8*3)^Integer.MIN_VALUE;
            all += (p2<<8*2);
            all += (p3<<8);
            all += p4;
            if( !Integer.toBinaryString(all).matches("1+0+")){
                result[ERROR_MASK]++;
            }
            if(!flag)continue;

            int part1 = Integer.parseInt(ip[0]);
            int part2 = Integer.parseInt(ip[1]);
            int part3 = Integer.parseInt(ip[2]);
            int part4 = Integer.parseInt(ip[3]);
            // 不计入的
            if(part1 == 0 || 127 == part3){
                continue;
            }
            if(1 <= part1 && part1 <= 126){
                result[A_IP]++;
                if(10 == part1){
                    result[PRIVATE_IP]++;
                }
            }else if(128 <= part1 && part1 <= 191){
                result[B_IP]++;
                if(172 == part1 && 16 <= part2 && part2 <= 31){
                    result[PRIVATE_IP]++;
                }
            }else if(192 <= part1 && part1 <= 223){
                if(192 == part1 && part2 == 168){
                    result[PRIVATE_IP]++;
                }else{
                    result[C_IP]++;
                }
            }else if(224 <= part1 && part1 <= 239){
                result[D_IP]++;
            }else if(240 <= part1 && part1 <= 255){
                result[E_IP]++;
            }else{
                result[ERROR_MASK]++;
            }

        }
        System.out.println(result[A_IP] + " " + result[B_IP]
                + " "+ result[C_IP]
                + " "+ result[D_IP]
                + " "+ result[E_IP]
                + " "+ (result[ERROR_IP] + result[ERROR_MASK])
                + " "+ result[PRIVATE_IP]
        );
    }
}
