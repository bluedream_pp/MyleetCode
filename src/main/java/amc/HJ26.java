package amc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @className: HJ26
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/30 15:46
 */
public class HJ26 {
    private static Logger logger = LoggerFactory.getLogger(HJ26.class);

    public static void main(String[] args) {
//        char[] chars = "Type".toCharArray();
        char[] chars = "BabA".toCharArray();
        System.out.println(Arrays.toString(sort(chars, 0, chars.length - 1)));
    }
    public static char[] sort(char[] nums , int start , int end){
        if(start >= end){
            return new char[]{nums[start]};
        }
        int mid = (start+end)/2;
        char[] p1 = sort(nums , start , mid);
        char[] p2 = sort(nums , mid + 1, end);
        char[] rs = new char[p1.length + p2.length];
        int idx1 = 0 ;
        int idx2 = 0 ;
        int idx3 = 0 ;
        while(idx1 < p1.length && idx2 < p2.length){
            if(compare(p1[idx1] , p2[idx2]) >= 0){
                rs[idx3++] = p1[idx1++];
            }else{
                rs[idx3++] = p2[idx2++];
            }
        }
        while(idx1 < p1.length){
            rs[idx3++] = p1[idx1++];
        }
        while(idx2 < p2.length){
            rs[idx3++] = p2[idx2++];
        }
        return rs ;
    }
    public static int compare(char i1 , int i2){
        int a1 = (i1 >= 'a'? i1 : i1 + ('a' - 'A') );
        int a2 = (i2 >= 'a'? i2 : i2 + ('a' - 'A') );
        return a2 - a1 ;
    }
}
