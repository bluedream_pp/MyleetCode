package amc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @className: number6
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/3 21:37
 */
public class number6 {
    private static Logger logger = LoggerFactory.getLogger(number6.class);

    public static void main(String[] args) {
        Map<String,Integer> map = new LinkedHashMap();
        map.put("l1" , 1);
        map.put("l2" , 1);
        map.put("l3" , 1);
        map.put("l4" , 1);
        map.put("l5" , 1);
        map.put("l6" , 1);
        map.put("l7" , 1);
        map.put("l8" , 1);
        map.put("l9" , 1);
        map.put("l0" , 1);
        int count = 0;
        for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
            if(map.size() - count <= 8){
                System.out.println(stringIntegerEntry.getKey() + " " + stringIntegerEntry.getValue());
            }
            count++;

        }
    }
}
