package amc;

import java.util.Scanner;

import static JUC.A1B2C3.A1B2C3ReentrantLockCondition.i;

/**
 * @className: number5
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/28 12:44
 */
public class number5 {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        while(scanner.hasNextLine()){
//            String L = scanner.nextLine();
//            String S = scanner.nextLine();
//            System.out.println(isFullContain(L , S));
//        }
//        System.out.println(randString(10));
        for (int j = 0; j < 91000; j++) {
            int i = randInt(10);
            while(i<=0){
                i = randInt(10);
            }
            String L = randString(i);
            i = randInt(10);
            while(i<=0){
                i = randInt(10);
            }
            String S = randString(i);
            if(process(L, S) != isFullContain(L , S)){
                System.out.println("Ops, L:" + L +" , S:" + S);
            }
        }
    }
    public static char randChar(){
        int i = randInt(1);
        while(i > 26){
            i = randInt(1);
        }
        return (char)(i + 'a');

    }
    public static int randInt(int n){
        return (int) (Math.random() * 10 * n);
    }
    public static String randString(int n){
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < n ; i++){
            sb.append(randChar());
        }
        return sb.toString();
    }
    public static int process(String L , String S){
        int p = 0 ;
        for (int j = 0; j < L.length(); j++) {
            int i = S.indexOf(L.charAt(j));
            p = (i >= p ? i : -1 );
            if(p == -1){
                return -1 ;
            }
        }
        return p ;
    }
    public static int isFullContain(String L , String s){
         char[] chs = L.toCharArray();
         int[] pos = new int[chs.length];
         for(int i = 0 ; i < chs.length ; i++){
             int i1 = s.indexOf(chs[i]);
             if(i1 == -1){
                 return -1 ;
             }else{
                 pos[i] = i1;
             }
         }
         for(int i = 1 ; i < pos.length ; i++){
            if(pos[i-1] > pos[i]){
                return -1 ;
            }
         }
         return pos[pos.length -1];
    }
}
