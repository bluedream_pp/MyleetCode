package amc;


import java.util.Scanner;

/**
 * @className: number4
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/24 11:33
 */
public class number4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        String[] split = s.split("\\s+");
        int w = Integer.parseInt(split[0]);
        int h = Integer.parseInt(split[1]);
        int x = Integer.parseInt(split[2]);
        int y = Integer.parseInt(split[3]);
        int sx = Integer.parseInt(split[4]);
        int sy = Integer.parseInt(split[5]);
        int t = Integer.parseInt(split[6]);
        int[][] road = new int[h][w];
        for(int i = 0 ; i < h ; i ++){
            String line = scanner.nextLine();
            char[] chars = line.toCharArray();
            for (int j = 0; j < chars.length; j++) {
                road[i][j] = chars[j];
            }
        }
        Car car = new Car(road , new Point(sx , sy) , new Point(x , y));
        for(int i = 0 ; i < t ; i ++){
            car.move();
        }
        if('1' == road[car.position.y][car.position.x]){
            car.score++;
        }
        System.out.println(car.score);

    }
    public static class Point {
        public int x ;
        public int y ;
        public Point(int x , int y){
            this.x = x ;
            this.y = y ;
        }
    }
    public static class Car{
        public int[][] road ;
        public Point direction ;
        public Point position ;
        public int score ;
        public Car(int[][] road , Point direction , Point position){
            this.road = road ;
            this.direction = direction;
            this.position = position;
        }
        public void move(){
            if('1' == road[position.y][position.x]){
                score++;
            }
            if(position.x  == 0 || position.x  == road[0].length-1){
                direction.x = - direction.x;
            }
            if(position.y == 0 || position.y == road.length-1){
                direction.y = - direction.y;
            }
            position.x += direction.x ;
            position.y += direction.y ;

        }
    }
}
