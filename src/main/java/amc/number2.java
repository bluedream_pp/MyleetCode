package amc;

/**
 * @className: number2
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/23 14:09
 */
public class number2 {
    public static void main(String[] args) {

        // 这个方法没有优先级
        System.out.println(process1("7#6$5#12"));
        // 需要一个优先级 # 优先级大于 #
        System.out.println(process2("((2+3)*3)*(4+1)"));
    }
    /**
     * 加上 （） 的优先级 , 只有 + *
     * */
    public static int process2(String input){
        int index = input.lastIndexOf("(");
        if(-1 != index){
            return process2( input.substring(0 , index) + process2(input.substring(index + 1)));
        }
        index = input.lastIndexOf(")");
        if(-1 != index){
            return process2( process2(input.substring(0 , index)) + input.substring(index+1));
        }
        index = input.lastIndexOf("+");
        if(-1 != index){
            return process2(input.substring(0 , index)) + process2(input.substring(index + 1));
        }
        index = input.lastIndexOf("*");
        if(-1 != index){
            return process2(input.substring(0 , index)) * process2(input.substring(index + 1));
        }
        return Integer.parseInt(input);
    }
    public static int process1(String input){
        int index = input.lastIndexOf("$");
        if(-1 != index){
            return 4*process1(input.substring(0 , index)) + 3*process1(input.substring(index + 1 )) + 2 ;
        }
        index = input.lastIndexOf("#");
        if(-1 != index){
            return 2*process1(input.substring(0 , index)) + process1(input.substring(index + 1 )) + 3 ;
        }
        return Integer.parseInt(input);
    }
    public static void process(String input){
        char[] chars = input.toCharArray();
        String[] numbers = input.split("[#\\$]");
        int sum = 0 ;
        int index = 0 ;
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i  < chars.length ; i++){
            if(chars[i] == '#' || chars[i] == '$' ){
                sum += compute(chars[i]
                        ,(0 == index ? Integer.parseInt(numbers[0]) : sum)
                        ,Integer.parseInt(numbers[index+1])
                        ) ;
                index++;
            }
        }
        System.out.println(sum);
    }
    public static int compute(char c , int num1 , int num2){
        int rs = 0 ;
        if(c == '#'){
           rs = 4*num1 + 3*num2 + 2 ;
        }else if(c == '$'){
            rs = 2*num1 + num2 + 3 ;
        }
        return rs ;
    }
}
