package amc;


/**
 * @className: number1
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/23 13:28
 */
public class number1 {
    /**
     * 用连续的自然数之和。
     * */
    public static void main(String[] args) {
        // 使用等差和公式
        // S = n*(A1 + An)/2 = n*(A1 + A1 + (n-1))/2 = n*(2A1 + n-1)/2
        // (S - n(n-1)/2)/n = A1
        process(9);
        process(6);
        process(7);
    }
    public static void process(int num){
        if(num < 0) return ;
        double a1 = 0.0D ;
        int sum = 1 ;
        double ii = 0.0D ;
        double rs = 0.0D ;
        System.out.println(num);
        for(int i = 2 ; i <= num ; i++){
            ii = (double) i;
            rs = (double)num - ii*(ii-1)/2;
            a1 = (0 == (rs)%i ? rs/i : -1) ;
            if(a1 > 0 ){
                print(i , (int)a1 , num);
                sum++;
            }
        }
        System.out.println("Result:" + sum);
    }
    public static void print(int len , int a1 , int num){
        System.out.print(num + "=");
        for(int i = 0 ; i < len - 1 ; i++ ){
            System.out.print((a1 + i) + "+" );
        }
        System.out.print(a1 + len - 1);
        System.out.println();
    }
}
