package amc;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String expression = in.nextLine();
            System.out.println(process(expression));
        }
    }
    public static final String PLUS = "+";
    public static final String M = "-";
    public static final String START = "*";
    public static final String D = "/";
    public static final String L = "(";
    public static final String R = ")";

    public static int process(String expression){
        int position = expression.lastIndexOf(L);
        if(-1 != position){
            String sub = expression.substring(position+1);
            char[] chars = sub.toCharArray();
            int cnt = 0 ;
            int i = 0 ;
            while (!(cnt == 0 && chars[i] == ')')){
                if('(' == chars[i]){
                    cnt++;
                }else if(')' == chars[i]){
                    cnt--;
                }
                i++;
            }
            String rs = process(sub.substring(0, i)) + "";
            return process(expression.substring(0 , position) + rs + (i == sub.length()-1? "" : sub.substring(i+1)));
        }
//        position = expression.lastIndexOf(R);
//        if(-1 != position){
//            return process(process(expression.substring(0 , position)) + expression.substring(position+1));
//        }
        position = expression.lastIndexOf(PLUS);

        if(expression.matches("\\-\\d+")){
            return Integer.parseInt(expression);
        }
        int p2 = expression.lastIndexOf(M);
        int startIndex = (-1 == p2 ? -1 : p2 -1) ;
        while( startIndex>=0 && p2 >= 1 && !('0' <= expression.charAt(p2-1) && expression.charAt(p2-1) <= '9' && '-' == expression.charAt(p2))  ){
            p2 = expression.lastIndexOf(M , startIndex);
            if(-1 == p2) break ;
            startIndex = p2 - 1 ;
        }
        if(p2 == 0){
            p2 = -1 ;
        }
        if(position > p2 && -1 != position){
            return process(expression.substring(0 , position)) + process(expression.substring(position+1));
        }
        if(position < p2 && -1 != p2){
            return process(expression.substring(0 , p2)) - process(expression.substring(p2+1));
        }

        position = expression.lastIndexOf(START);
        p2 = expression.lastIndexOf(D);
        if(position > p2 && -1 != position){
            return process(expression.substring(0 , position)) * process(expression.substring(position+1));
        }
        if(position < p2 && -1 != p2){
            return process(expression.substring(0 , p2)) / process(expression.substring(p2+1));
        }
        return Integer.parseInt(expression);
    }
}