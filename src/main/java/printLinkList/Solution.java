package printLinkList;

import java.util.ArrayList;

public class Solution {
    public ArrayList<Integer> printListFromTailToHead(ListNode listNode){
        ArrayList<Integer> rs = new ArrayList<>() ;
        ListNode next = null ;
        ListNode pre = null ;

        if(null == listNode){
            System.out.println("list node must not be emputy");
            return null ;
        }

        if(null == listNode.getNext()){
            rs.add(listNode.getVal());
            return rs ;
        }
        if(null == listNode.getNext().getNext()){
            rs.add(listNode.getVal());
            rs.add(listNode.getNext().getVal());
            return rs;
        }

        pre = listNode.getNext() ;
        next = listNode.getNext().getNext();
        pre.setNext(listNode);
        listNode.setNext(null);

        ListNode cur = null ;
        while(next != null){
            cur = next;
            next = next.getNext();
            cur.setNext(pre);
            pre = cur ;
            System.out.println("val:" + (null == next?"":next.getVal()));
        }

        do{
            rs.add(pre.getVal());
        } while((pre = pre.getNext()) != null);

        return rs ;
    }
}
