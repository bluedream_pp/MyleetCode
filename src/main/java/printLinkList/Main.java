package printLinkList;
/*
* 输入一个链表，按链表从尾到头的顺序返回一个ArrayList。
*
*输入
*{67,0,24,58}
*返回值
*[58,24,0,67]
*
* */
public class Main {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
//        ListNode l4 = new ListNode(4);
//        ListNode l5 = new ListNode(5);
        l1.setNext(l2);
        l2.setNext(l3);
//        l3.setNext(l4);
//        l4.setNext(l5);
        for(Integer e :new Solution().printListFromTailToHead(l1)){
            System.out.println(e);
        }
    }
}
