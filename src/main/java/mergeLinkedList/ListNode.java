package mergeLinkedList;

public class ListNode {
    private int var = 0 ;
    private ListNode next ;

    public ListNode(int var) {
        this.var = var;
    }

    public int getVar() {
        return var;
    }

    public void setVar(int var) {
        this.var = var;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }
}
