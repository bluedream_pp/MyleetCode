package findDuplicateNumber;

public class Solution {
    private int[] a = new int[]{1,3,1,0,1,5,3};
    private long bitMap = 0 ;
    public int[] getDuplicateNumbers(){
        int[] rs = new int[10];
        int idx = 0 ;
        for( int e : a){
            if((bitMap & (1<< e)) > 0){
                rs[idx++] = e ;
            }else{
                bitMap = bitMap | (1 << e);
            }
        }
        return rs;
    }
    public int[] getDuplicateNumbers1(){
       int[] rs = new int[10] ;
       int idx = 0 ;
       for(int i = 0 ; i < a.length ; i++){
           while( i != a[i]  && a[a[i]] != a[i]){
             swap(this.a  , i , a[i]);
           }
           if( i != a[i]  &&  a[a[i]] == a[i]){
             rs[idx++] = a[i];
           }
       }
       return rs ;
    }
    private void swap( int[] a , int start , int end ){
       int swap = 0 ;
       swap = a[start];
       a[start] = a[end];
       a[end] = swap;
    }
    public void invokerDuplicate(){
        int[] b = new int[10];
        System.out.println(duplicate(a , a.length , b));
    }
    boolean duplicate(int numbers[], int length, int[] duplication) {
        for(int i=0;i<length;i++){
            int index = numbers[i];
            if(numbers[index>0?index:-1*index]<0){
                duplication[0]=numbers[index>0?index:-1*index]*-1;
                return true;
            }
            numbers[index]*=-1;
        }
        return false;
    }
}
