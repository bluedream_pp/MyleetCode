package lockTest;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReaderWriterLockTest {
    public static void main(String[] args) throws InterruptedException {
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
//        ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
        ReentrantReadWriteLock.ReadLock readLock = lock.readLock();

        // 下面是写锁的情况
        /*
        Thread t2 = new Thread(() -> {
            writeLock.lock();
        });

        Thread thread = new Thread(() -> {
            writeLock.lock();
            t2.start();
            writeLock.unlock();
        });
        thread.start();
         */
        // 下面是读锁的 acquir 和 release
        readLock.lock();
        readLock.lock();
        readLock.unlock();

    }
}
