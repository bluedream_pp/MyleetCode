## 题目说明

实现一个容器，提供两个方法， add 和 size 
写两个线程，线程1添加 10 个元素到容器中，线程2 实现监控元素的个数，当个数到 5 个的时候，线程2 给出提示并结束

## 解题思路

1. synchronized
2. CountdowLatch
3. Semaphore
4. CycliBarrier 
5. Phaseer
6. LockSupport unpark() park()


synchronized 是可以实现的，但是，需要让 线程2 先运行起来，等待线程1加到第 4 个数字。
synchronize 的 notify() 函数只能唤醒 WaitSet 里面的线程。如果当时线程2没有在等待
线程中，线程2将不能被唤醒了。


