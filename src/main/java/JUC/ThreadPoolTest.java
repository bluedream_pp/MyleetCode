package JUC;

import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolTest {
    public static void main(String[] args) {
        ArrayBlockingQueue workQueue = new ArrayBlockingQueue(1);
        AtomicInteger ai = new AtomicInteger();
        ThreadPoolExecutor pool = new ThreadPoolExecutor(
                //
                2
                , 3
                , 0
                , TimeUnit.SECONDS
                , workQueue
                , x -> {
            return new Thread("test" + ai.getAndIncrement());
        }
                , new ThreadPoolExecutor.AbortPolicy()
        );
        Runnable r1 = ()->{
            try {
                Thread.sleep(10000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Runnable r2 = ()->{
            try {
                Thread.sleep(10000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        pool.submit(r1);
        pool.submit(r2);
        pool.submit(()->{
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        pool.submit(()->{
            try {
                Thread.sleep(1000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
//        pool.submit(()->{
//            try {
//                Thread.sleep(1000000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
        System.out.println(pool.getPoolSize());

        pool.shutdown();

    }
}
