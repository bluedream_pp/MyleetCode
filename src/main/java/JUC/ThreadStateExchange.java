package JUC;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @className: ThreadStateExchange
 * @Description:
 * 写程序，延时一下各个状态之间的转换。
 * READY
 * @Author: wangyifei
 * @Date: 2022/8/27 21:55
 */
public class ThreadStateExchange {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();

        ReentrantReadWriteLock lock1 = new ReentrantReadWriteLock();

        ReentrantReadWriteLock.WriteLock writeLock = lock1.writeLock();
        ReentrantReadWriteLock.ReadLock readLock = lock1.readLock();

        writeLock.lock();
        writeLock.unlock();
        writeLock.newCondition();

        readLock.lock();
        readLock.unlock();
        Condition condition1 = readLock.newCondition();

        Condition condition = lock.newCondition();
        lock.lock();
        try{
            try {
                condition.await();
                condition.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("xx");
        }finally {
            lock.unlock();
        }

    }

}
