package JUC;

public class ThreadState {
    public static void main(String[] args) {
//        RUNNABLE_BLOCKED();
        RUNNABLE_WAITING();
//        RUNNABLE_TIME_WAITIONG2();
    }
    /**
     * 使用 sleep 函数，使线程进入 TIME_WAITING 的函数
     * */
    public static void RUNNABLE_TIME_WAITIONG2(){
        Thread t1 = new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getState());
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("t1 进入 sleep 后 ，t1 的状态是：" + t1.getState());
        t1.stop();
    }
    /**
     * 通过 wait 函数，使线程变成 WAITING 状态，
     * notify 函数，使线程变成 RUNNABLE 状态
     * */
    public static void RUNNABLE_WAITING(){
        /**
         * 当 RUNNABLE 状态的线程，执行 wait 函数的时候，会进入 WAITING 状态
         * */
        Object lock = new Object();
        Thread t1 = new Thread(()->{
            synchronized (lock){
                try {
                    lock.wait(1000000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(()->{
            synchronized (lock){
                try {
                    lock.notify();
                } catch ( Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        t2.start();
        System.out.println("t1 start 执行后，t1 的状态是：" + t1.getState() + " t2 的状态是：" + t2.getState());
        System.out.println("t1 进入同步代码块之后，执行 wait() ，t1 的状态是：" + t1.getState() + " t2 的状态是：" + t2.getState());
    }

    public static void RUNNABLE_BLOCKED(){
       /**
        * 当 RUNNABLE 状态的线程，没有抢到锁的情况下，进入 BLOCKED 状态
        * */
       Thread t1 = new Thread(()->{
           System.out.println("");
           synchronized (ThreadState.class){
               System.out.println("t1");
           }
       },"t1");

        Thread t2 = new Thread(()->{
            System.out.println("");
            synchronized (ThreadState.class){
                System.out.println("t2");
            }
        },"t2");
        System.out.println("t1 被 new 出来之后，但是没有执行 start 函数，t1 的状态是：" + t1.getState() + " , t2 的状态: " + t2.getState());
        t1.start();
        t2.start();
        System.out.println("执行 start 后，t1 的状态是：" + t1.getState() + " , t2 的状态: " + t2.getState());
        System.out.println("------");
        System.out.println("t1 获得锁之后，t1 的状态是：" + t1.getState() + " , t2 的状态: " + t2.getState());
    }

    public static void NEW_RUNNABLE_TERMINAL() {
        /**
         * java 的五种状态：
         * NEW
         * RUNNABLE
         * BLOCKED
         * WAITING
         * TIMED_WAITING
         * TERMINATED
         * */
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println();
            }
        });
        System.out.println(" new 出来 Thread ，但是没有执行它的 state 函数，t1 state: " + t1.getState());
        System.out.println("----------");
        t1.start();
        System.out.println("执行 start 后，t1 的状态： " + t1.getState());
        System.out.println("------------");
        System.out.println("t1 执行完 run 方法之后，t1 的状态是：" + t1.getState());
    }

}
