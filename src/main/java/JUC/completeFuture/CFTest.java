package JUC.completeFuture;

import jdk.nashorn.internal.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * @className: CFTest
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/22 9:27
 */
public class CFTest {
    private static Logger logger = LoggerFactory.getLogger(CFTest.class);

    public static void main(String[] args) {
//        testCompletableFuture();
//        testTwoDependency();
//        testBoth();
        handleExeception();
    }
    public static void testSerial(){
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            return 12;
        });
        // 1 -> 2 ，这种依赖关系。其实可以使用 CountdownLatch 来实现。 初始化值设置为 1
        CompletableFuture<Integer> future1 = future.thenCompose(x -> {
            return CompletableFuture.supplyAsync(()-> {
                return 11*x;
            });
        });
        System.out.println(future1.join());
        LockSupport.parkNanos(10000);
    }
    public static void testTwoDependency(){
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
                    return "hello ";
                })
                .thenCombine(CompletableFuture.supplyAsync(() -> {
                            return " world";
                        })
                        , (result1, result2) -> {
                            return result1 + result2;
                        }
                );
        System.out.println(future.join());
    }
    public static void testBoth(){
        CompletableFuture<Void> future  = CompletableFuture.supplyAsync(() -> {
                    return 22;
                })
                .thenAcceptBoth(
                        CompletableFuture.supplyAsync(() -> {
                            return 22;
                        })
                        , (r1, r2) -> {
                            System.out.println(r1 + r2);
                        }
                );
        LockSupport.park();
    }
    public static void handleExeception(){
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            int i = 9/0;
            return 12;
        });
//        future.whenComplete((futureResult , t) -> {
//            if(null != t){
//                System.out.println("exception: " + t.getMessage());
//            }
//            System.out.println(futureResult);
//        });
        future.handle((rs , ex) -> {
           Map<String , String> result = new HashMap<>();
           if(null != ex){
               result.put("code" , "001");
               result.put("msg" , "" + ex.getMessage());
               return result ;
           }
            result.put("code" , "200");
            result.put("msg" , "success");
            return result ;
        }).thenAccept((x)->{
            System.out.println(x);
        });
        LockSupport.park();
    }
}
