package JUC.A1B2C3;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @className: A1B2C3
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 17:26
 */
public class A1B2C3Synchronized {

    public static int i = 1 ;
    public static void main(String[] args) {
        Object lock = new Object();
        Runnable r1 = ()->{

            while(true){
                synchronized (lock){
                    while(A1B2C3Synchronized.i != 1){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    A1B2C3Synchronized.i = 2 ;
                    System.out.println("A");
                    lock.notify();
                }
            }
        };

        Runnable r2 = ()->{
            while(true){
                synchronized (lock){
                    while(i != 2){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("B");
                    A1B2C3Synchronized.i = 3 ;
                    lock.notify();
                }
            }
        };

        Runnable r3 = ()->{
            while(true){
                synchronized (lock){
                    while(i != 3){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("C");
                    A1B2C3Synchronized.i = 1 ;
                    lock.notify();
                }
            }
        };
        ThreadPoolExecutor pool = new ThreadPoolExecutor(3 , 3 , 1000
                , TimeUnit.SECONDS
                , new ArrayBlockingQueue<>(100));
        pool.execute(r1);
        pool.execute(r2);
        pool.execute(r3);
        pool.shutdown();
    }
}
