package JUC.A1B2C3;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @className: SolutionBlockingQueue
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 14:38
 */
public class SolutionBlockingQueue {
    public static void main(String[] args) {
        ArrayBlockingQueue<Integer> queue1 = new ArrayBlockingQueue(1);
        ArrayBlockingQueue<Integer> queue2 = new ArrayBlockingQueue(1);
        queue1.add(1);
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();
        ExecutorService service = Executors.newFixedThreadPool(2);
        Runnable r1 = () ->{
            for(char a : aI){
                try {
                    queue1.take();
                    System.out.print(a);
                    queue2.put(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Runnable r2 = () ->{
            for(char a : aC){
                try {
                    queue2.take();
                    System.out.print(a);
                    queue1.put(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        service.execute(r1);
        service.execute(r2);
        service.shutdown();
    }
}
