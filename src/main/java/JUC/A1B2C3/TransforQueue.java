package JUC.A1B2C3;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedTransferQueue;

/**
 * @className: TransforQueue
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 16:19
 */
public class TransforQueue {
    public static void main(String[] args) {
        LinkedTransferQueue<Integer> q1 = new LinkedTransferQueue<>();
        LinkedTransferQueue<Integer> q2 = new LinkedTransferQueue<>();
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();

        Runnable r1 = () -> {
            for(char a : aI){
                try {
                    System.out.print(a);
                    q1.take();
                    q2.transfer(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Runnable r2 = () -> {
            for(char a : aC){
                try {
                    q1.transfer(1);
                    System.out.print(a);
                    q2.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(r1);
        service.execute(r2);
        service.shutdown();
    }
}
