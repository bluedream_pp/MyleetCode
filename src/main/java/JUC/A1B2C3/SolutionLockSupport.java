package JUC.A1B2C3;


import JUC.printWhen5.LockSupportList;

import java.util.concurrent.locks.LockSupport;

/**
 * @className: SolutionLockSupport
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 11:24
 */
public class SolutionLockSupport {
    public static Thread t1 ;
    public static Thread t2 ;
    public static volatile boolean which = false ;
    public static void main(String[] args) {
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();
        t1 = new Thread(() -> {
                for(char a : aI){
                    if(which){
                        LockSupport.park();
                    }
                    System.out.print(a);
                    which = !which ;
                    LockSupport.unpark(t2);
                }
        });
        t2 = new Thread(()->{
                for(char a : aC){
                    while(!which){
                        LockSupport.park();
                    }
                    System.out.print(a);
                    which = !which ;
                    LockSupport.unpark(t1);
                }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
