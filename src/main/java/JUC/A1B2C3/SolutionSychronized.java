package JUC.A1B2C3;


import java.io.IOException;
import java.util.Arrays;

/**
 * @className: SolutionSychronized
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 11:11
 */
public class SolutionSychronized {
    public static volatile boolean which = false ;
    public static void main(String[] args) throws IOException {
        Object lock = new Object();

        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();
        Thread t1 = new Thread(()->{
            synchronized (lock){
                for(char a : aI){
                    while(which){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    which = !which;
                    System.out.print(a);
                    lock.notify();
                }
                lock.notify();
            }
        });
        Thread t2 = new Thread(()->{
            synchronized (lock){
                for(char a : aC){
                    while(!which){
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    which = !which;
                    System.out.print(a);
                    lock.notify();
                }
                lock.notify();
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}