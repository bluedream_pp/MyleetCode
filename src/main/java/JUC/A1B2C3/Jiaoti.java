package JUC.A1B2C3;


import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @className: Jiaoti
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 15:44
 */
public class Jiaoti {
    public static void main(String[] args) throws IOException {
        CountDownLatch cdl1 = new CountDownLatch(1);
        CountDownLatch cdl2 = new CountDownLatch(1);
        new Thread(()->{
            while(true){
                try {
                    cdl1.await();
                    System.out.println("A");
                    Thread.sleep(1000);
                    cdl2.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        new Thread(()->{
            while(true){
                try {
                    cdl1.countDown();
                    System.out.println("B");
                    Thread.sleep(1000);
                    cdl2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        System.in.read();
    }
}
