package JUC.A1B2C3;


import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @className: SolutionExchage
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 15:16
 */
public class SolutionExchage {
    public static void main(String[] args) {
        Exchanger<Integer> exchanger = new Exchanger<>();
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();
        Runnable r1 = () -> {
            try {
                for(char a : aI){
                    exchanger.exchange(1);
                    System.out.print(a);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Runnable r2 = ()->{
            try {
                for(char a : aC){
                    exchanger.exchange(1);
                    System.out.print(a);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(r1);
        service.execute(r2);
        service.shutdown();
    }
}
