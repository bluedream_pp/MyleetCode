package JUC.A1B2C3;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * @className: Semaphore
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 16:03
 */
public class SolutionSemaphore {
    public static void main(String[] args) {
        Semaphore s1 = new Semaphore(1);
        Semaphore s2 = new Semaphore(0);

        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();
        Runnable r1 = () ->{
            for(char a : aI){
                try {
                    s1.acquire();
                    System.out.print(a);
                    s2.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable r2 = () ->{
            for(char a : aC){
                try {
                    s2.acquire();
                    System.out.print(a);
                    s1.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(r1);
        service.execute(r2);
        service.shutdown();
    }
}
