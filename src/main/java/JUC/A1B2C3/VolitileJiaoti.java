package JUC.A1B2C3;


import java.io.IOException;

/**
 * @className: VolitileJiaoti
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 15:51
 */
public class VolitileJiaoti {
    public static volatile boolean ab = true ;
    public static void main(String[] args) {
//        Object lock = new Object();
        new Thread(() -> {
            while(true){
                if(ab){
                    System.out.println("A");
                    ab = !ab ;
                }else{
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }
            }
        }).start();

        new Thread(() -> {
            while(true){
                if(!ab){
                    System.out.println("B");
                    ab = !ab ;
                }else{
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }
            }
        }).start();

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
