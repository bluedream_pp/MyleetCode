package JUC.A1B2C3;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedTransferQueue;

/**
 * @className: SolutionTransforQueue
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 16:56
 */
public class SolutionTransforQueue {

    public static void main(String[] args) {
        LinkedTransferQueue<Character> queue = new LinkedTransferQueue<>();
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();

        Runnable r1 = () -> {
            for(char a : aI){
                try {
                    queue.transfer(a);
                    System.out.print(queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Runnable r2 = () -> {
            for(char a : aC){
                try {
                    System.out.print(queue.take());
                    queue.transfer(a);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(r1);
        service.execute(r2);
        service.shutdown();
    }
}
