package JUC.semaphoreUsage;


import java.util.concurrent.Semaphore;

/**
 * @className: NodeManager
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 10:37
 */
public class NodeManager implements Runnable{
    private Semaphore s ;
    public NodeManager(Semaphore s){
        this.s = s ;
    }

    @Override
    public void run() {
        try {
            this.s.acquire();
            System.out.println("获取许可 , 开始干活。。。");
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            this.s.release();
        }
    }
}
