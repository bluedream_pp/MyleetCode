package JUC.semaphoreUsage;


import java.io.IOException;
import java.util.concurrent.*;

/**
 * @className: ResourceManager
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 10:23
 */
public class ResourceManager {
    private Semaphore semaphore ;
    public ResourceManager(int permits){
        semaphore = new Semaphore(permits);
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public static void main(String[] args) {
        ResourceManager resourceManager = new ResourceManager(5);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100));
        for(int i = 0 ; i < 20 ; i++){
            threadPoolExecutor.execute(new NodeManager(resourceManager.getSemaphore()));
        }
        threadPoolExecutor.shutdown();
    }
}
