## 题目

谢一个固定容量的同步容器，荣有 put 和 get 方法，以及 getCount 方法，
能够支持 2 个生产者线程以及 10 个消费者线程的阻塞调用。

## 实现思路

1. 使用 wait/notify/notifyAll 来实现。
2. 使用 ReentrantReadWriteLock ，读锁和写锁配合。
3. ReentrantLock Condition 等待队列的实现方式。

