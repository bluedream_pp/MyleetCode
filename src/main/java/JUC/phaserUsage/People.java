package JUC.phaserUsage;


import java.util.concurrent.Phaser;

/**
 * @className: People
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 9:42
 */
public abstract class People implements Runnable{
    protected Phaser phaser ;
    protected String name ;
    public abstract void arrive();
    public abstract void takePic();
    public abstract void eat();
    public abstract void dongfang();

    public void setName(String name) {
        this.name = name;
    }

    public void setPhaser(Phaser phaser) {
        this.phaser = phaser;
    }

    @Override
    public void run() {
        arrive();

        takePic();

        eat();

        dongfang();
    }
}
