package JUC.phaserUsage;


/**
 * @className: MarryGuy
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 9:51
 */
public class MarryGuy extends People{

    @Override
    public void arrive() {
        System.out.println(name + " arrived");
        phaser.arriveAndAwaitAdvance();
    }

    @Override
    public void takePic() {
        System.out.println(name + " pose is ok ");
        phaser.arriveAndAwaitAdvance();
    }

    @Override
    public void eat() {
        System.out.println(name + " eat");
        phaser.arriveAndAwaitAdvance();
    }

    @Override
    public void dongfang() {
        System.out.println("come in");
        phaser.arriveAndAwaitAdvance();
    }
}
