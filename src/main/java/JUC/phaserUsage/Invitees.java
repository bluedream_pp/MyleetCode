package JUC.phaserUsage;


/**
 * @className: Invitees
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 9:45
 */
public class Invitees extends People{

    @Override
    public void arrive() {
        System.out.println(name + " arrived");
        phaser.arriveAndAwaitAdvance();
    }

    @Override
    public void takePic() {
        System.out.println(name + " pose is ok ");
        phaser.arriveAndAwaitAdvance();
    }

    @Override
    public void eat() {
        System.out.println(name + " eat");
        phaser.arriveAndDeregister();
    }

    @Override
    public void dongfang() {
    }

}
