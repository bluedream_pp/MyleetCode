package JUC.phaserUsage;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;

/**
 * @className: Phase
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/4 9:35
 */
public class MarryRoutine extends Phaser {
    @Override
    protected boolean onAdvance(int phase, int registeredParties) {
        switch (phase){
            case 0 :
                System.out.println("一共来了" + registeredParties + "个人");
                return false;
            case 1 :
                System.out.println("大家来合影 " + registeredParties + " . ");
                return false;
            case 2 :
                System.out.println("人来齐，开席了" + registeredParties);
                return false;
            case 3 :
                System.out.println("洞房了 " + registeredParties);
                return false;
            default:
                return true;
        }
    }

    public static void main(String[] args) {
        List<People> l = new ArrayList<>();
        People p = null ;
        MarryRoutine mr = new MarryRoutine();
        mr.bulkRegister(12);
        for (int i = 0 ; i < 10 ; i++){
            p = new Invitees();
            p.setPhaser(mr);
            p.setName("客人" + i);
            l.add(p);
            new Thread(p).start();
        }
        MarryGuy xinniang = new MarryGuy();
        xinniang.setName("新娘");
        xinniang.setPhaser(mr);
        new Thread(xinniang).start();
        MarryGuy xinlang = new MarryGuy();
        xinlang.setName("新郎");
        xinlang.setPhaser(mr);
        new Thread(xinlang).start();
    }
}
