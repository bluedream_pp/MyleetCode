package algorthm.sqlparser.CollectionTest;

import java.util.*;

public class TreeMapTest {
    public static void main(String[] args) {
        TreeMap<String,String> tree = new TreeMap<>();
        tree.put("1","1");
        tree.put("3","3");
        tree.put("2","2");
        tree.put("4","4");
        System.out.println("-------对map 中的 key 取倒序");
        NavigableSet<String> strings = tree.descendingKeySet();
        for (String string : strings) {
            System.out.println(string);
        }
        System.out.println("-------对map取倒序");
        // 将整个 map 的顺序按照 key 值进行一个倒序
        NavigableMap<String, String> descendingMap = tree.descendingMap();
        Iterator<String> iterator = descendingMap.keySet().iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(String.format("key is %s , value is %s" , next , descendingMap.get(next)));
        }
        System.out.println("-------取前 n 个 entry , 类似的还有 subMap ， tailMap");
        SortedMap<String, String> head = tree.headMap("3" , true);
        Iterator<String> iterator1 = head.keySet().iterator();
        while (iterator1.hasNext()) {
            String next = iterator1.next();
            System.out.println(String.format("key is %s , value is %s" , next , head.get(next)));
        }

    }
}
