package algorthm.sqlparser;

/**
 * @className: SqlNodType
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/23 19:58
 */
public enum SqlNodeType {
    SELECT("select"),
    FROM("from"),
    WHERE("where"),
    AND("and"),
    OR("or"),
    EQUEL("="),
    FIELD("field"),
    TABLE("table"),
    OPERATOR("OPERATOR")
    ;

    private String type;

    SqlNodeType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
