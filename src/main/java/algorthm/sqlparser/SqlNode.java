package algorthm.sqlparser;


/**
 * @className: SqlNode
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/23 19:57
 */
public class SqlNode {
    public SqlNodeType type;
    public String cnt ;
    public SqlNode left ;
    public SqlNode right ;
    public SqlNode(SqlNodeType type){
        this.type = type ;
    }
}
