package algorthm;


/**
 * @className: CodeBinary
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/3 20:02
 */
public class CodeBinary {
    public static void printBinary(int num){
//        StringBuilder sb = new StringBuilder();
//        for (int j = 0; j < 32; j++) {
//            sb.append((num & (1<<j)) == 0 ? "0":"1");
//        }
//        System.out.println(num + " 的二进制格式为：" + sb.reverse().toString());
        System.out.println( " 的二进制格式为：" + (1<<-1));
    }
    public static void main(String[] args) {
//        printBinary(8);
//        printBinary(1);
//        printBinary(16);
//        printBinary(17);
//        printBinary(19);
//        printBinary(20);
//        printBinary(Integer.MIN_VALUE);
        // 1 - 1  =>
        // 1 + 2 => 0001 + 0010 =?
        // & 找出需要进位的为
        // ^ 是相加等于 1 的地方，
        // 有 A 和 B 两个东西，
        // A ^ B = C  在 C 都相加等于为 1 的值
        // A & B = D  是需要进位的值
        int A = 1 ;
        int B = 3 ;
//        printBinary(A);
//        printBinary(B);
//        System.out.println("A ^ B :");
//        int C = A ^ B ;
//        printBinary(C);
//        int D = A & B ;
//        System.out.println("A & B :");
//        printBinary(D );
//        C = C ^ (D<<1) ;
//        printBinary(C);
//
//        D = C & D ;
//
//        printBinary(D);
//        printBinary(plusTwoInteger(1, 2));
//        printBinary(plusTwoInteger(2, 2));
//        printBinary(plusTwoInteger(17, 19));
//        printBinary(plusTwoInteger(50, 50));
        System.out.println((1<<8)-1);
    }
    public static int plusTwoInteger(int a , int b){
        // 异或是不需进位的位置
        int rs =  a ^ b;
        // 且是需要进位的位置
        int mid = a & b ;
        int rs_buff = 0 ;
        while(mid != 0){
            rs = (rs_buff = rs) ^ ( mid = mid << 1);
            mid = rs_buff & mid  ;
        }
        return rs ;
    }
}
