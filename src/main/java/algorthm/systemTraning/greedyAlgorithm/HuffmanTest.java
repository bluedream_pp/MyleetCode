package algorthm.systemTraning.greedyAlgorithm;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

public class HuffmanTest {
    public static void main(String[] args) {
        String input = "abcd";
        Map<Character , String> mapping1 = Huffman.huffmanCode(Huffman.huffmanTree(Huffman.getQueue(input)));
        Map<Integer , Character> mapping2 = new HashMap<>();
        mapping1.forEach((key , value) ->{
            mapping2.put(Integer.parseInt(value , 2) , key);
        });
        HuffmanEncoder.Info encode = HuffmanEncoder.encode(input, mapping1);
        String decode = HuffmanDecoder.decode(encode, mapping2);
        System.out.println("word: " + decode);
    }
}
