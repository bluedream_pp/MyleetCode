package algorthm.systemTraning.greedyAlgorithm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.BitSet;
import java.util.Map;

/**
 * @className: HuffmanEncoder
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/17 16:15
 */
public class HuffmanDecoder {
    private static Logger logger = LoggerFactory.getLogger(HuffmanDecoder.class);
    public static String decode(HuffmanEncoder.Info info, Map<Integer, Character> mapping){
        StringBuilder sb = new StringBuilder();
        int sum = 0 ;
        int i = 0 ;
        int j = 0 ;
        int end = 0 ;
        for (int k = 0; k < info.max; k++) {
            end += (1 << k);
        }
        while(i < info.bs.length()) {
            sum += (info.bs.get(i)? 1<<j : 0);
            j++;
            if(mapping.containsKey(sum) && j >= info.max-1){
                sb.append(mapping.get(sum));
                sum = 0 ;
                j = 0 ;
            }
            if(sum == end){
                break ;
            }
            i++;
        }
        return sb.toString();
    }
}
