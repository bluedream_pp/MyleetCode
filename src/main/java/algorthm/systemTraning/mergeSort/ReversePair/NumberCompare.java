package algorthm.systemTraning.mergeSort.ReversePair;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @className: NumberCompare
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/22 14:32
 */
public class NumberCompare {
    private static Logger logger = LoggerFactory.getLogger(NumberCompare.class);

    public static List<ReversePair.RPair> reversePair(int[] array , int start , int end){
        if(array == null || array.length == 0){
            return null;
        }
        List<ReversePair.RPair> rs = new ArrayList();
        for(int i = 0 ; i < array.length ; i++){
            for(int j = i + 1 ; j < array.length ; j++){
                 if(array[i] > array[j]){
                    rs.add(new ReversePair.RPair(array[i] , array[j]));
                 }
            }
        }
        return rs ;
    }
    public static void compare(int range , int size , int times){
        Random r = new Random();
        int arraySize = 0 ;
        int[] array = null ;
        int[] arrayBack = null ;
        for(int i = 0 ; i < times ; i++){
            arraySize = r.nextInt(size) + 2 ;
            array = null ;
            arrayBack = null ;
            array = new int[arraySize];
            arrayBack = new int[arraySize];
            for(int j = 0 ; j < arraySize ; j++){
                array[j] =  r.nextInt(range);
                arrayBack[j] = array[j] ;
            }
            List<ReversePair.RPair> r1 = reversePair(array, 0, arraySize - 1);
            List<ReversePair.RPair> r2 = ReversePair.mergeSort(array, 0, arraySize - 1);
            if(null == r1 && null == r2){
                continue;
            }
            if(r1 == null || r2 == null){
                System.out.println("-----" + Arrays.toString(arrayBack));
                continue;
            }
            Comparator<ReversePair.RPair> comparator = new Comparator<ReversePair.RPair>() {
                @Override
                public int compare(ReversePair.RPair o1, ReversePair.RPair o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            };
            if(r1.size() != r2.size()){
                System.out.println("size not equal:" + Arrays.toString(arrayBack));
                System.out.println(r1.stream().sorted(comparator).map(x->x.toString()).collect(Collectors.joining(",")));
                System.out.println(r2.stream().sorted(comparator).map(x->x.toString()).collect(Collectors.joining(",")));
            }else{

                String collect = r1.stream().sorted(comparator).map(x -> x.toString()).collect(Collectors.joining(","));
                String collect2 = r2.stream().sorted(comparator).map(x -> x.toString()).collect(Collectors.joining(","));
                if(!collect.equals(collect2)){
                    System.out.println("not equal:" + Arrays.toString(arrayBack));
                    System.out.println(collect);
                    System.out.println(collect2);
                }
            }
        }
    }

    public static void main(String[] args) {
        compare(1000 , 64 , 100000);
    }
}
