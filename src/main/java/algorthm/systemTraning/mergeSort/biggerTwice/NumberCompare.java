package algorthm.systemTraning.mergeSort.biggerTwice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Random;

/**
 * @className: NumberCompare
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/23 14:56
 */
public class NumberCompare {
    private static Logger logger = LoggerFactory.getLogger(NumberCompare.class);
    public static int biggerTwice(int[] array){
        int cnt = 0 ;
        for(int i = 0 ; i < array.length ; i++){
            for(int j = i + 1 ; j < array.length ; j++){
                if(array[i] > array[j]<<1){
                    cnt++;
                }
            }
        }
        return cnt ;
    }
    public static void compare(int range , int size , int times){
        int arraySize = 0 ;
        Random r = new Random();
        int[] array = null ;
        int[] arrayBackup = null ;
        for(int i = 0 ; i < size ; i++){
            arraySize = r.nextInt(size) + 2 ;
            array = new int[arraySize] ;
            arrayBackup = new int[arraySize] ;
            for(int j = 0 ; j < array.length ; j++){
                array[j] = r.nextInt(range);
                arrayBackup[j] = array[j] ;
            }
            if(biggerTwice(array) != BiggerTwice.mergeSort(array)){
                logger.info("error , {}" , Arrays.toString(arrayBackup));
            }
        }
    }

    public static void main(String[] args) {
        compare(100 , 102 , 123222);
    }
}
