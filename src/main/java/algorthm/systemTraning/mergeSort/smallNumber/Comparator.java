package algorthm.systemTraning.mergeSort.smallNumber;

import java.util.Arrays;
import java.util.Random;

/**
 * @className: Comparator
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/14 14:33
 */
public class Comparator {
    public static int smallNumber(int[] array){
       int ans = 0 ;
       for(int i = 0 ; i < array.length ; i++){
           for(int j = i -1; j >= 0 ; j--){
               if( array[j] < array[i]){
                 ans += array[j];
               }
           }
       }
       return ans ;
    }
    public static int[] randomArray(int numberRange , int sizeRange){
        Random r = new Random();
        sizeRange = r.nextInt(sizeRange) + 1;
        int[] ans = new int[sizeRange];
        for(int i = 0 ; i < sizeRange ; i++){
            ans[i] = r.nextInt(numberRange);
        }
        return ans ;
    }
    public static void compare(int numberRange , int sizeRange , int times){
        int[] array = null ;
        for(int i = 0 ; i < times ; i++){
            array = randomArray(numberRange, sizeRange);
            SmallNumber.smallNumber = -1;
            SmallNumber.mergeSort(array , 0 , array.length - 1);
            if(smallNumber(array) == SmallNumber.smallNumber){
                System.out.println(Arrays.toString(array));
            }
        }
    }

    public static void main(String[] args) {
//        int[] array = {1 , 3 , 5 , 9};
        // 1 + 4 + 9 = 14
//        int[] array = { 1 , 5 , 1 , 7 , 1 , 4 };
        // 1 + 7 + 3 = 11
//        System.out.println(smallNumber(array));
//        SmallNumber.mergeSort(array , 0 , array.length - 1);
//        System.out.println(SmallNumber.smallNumber);
        compare(160 , 240 , 1000000);
    }
}
