package algorthm.systemTraning.mergeSort.smallNumber;

import java.util.Arrays;

/**
 * @className: MergeSort
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/16 10:00
 */
public class MergeSort{
    public static void sort(int[] array , int L , int R){
//        if(array == null || array.length < 2){
//            return array ;
//        }
        int[] ans = new int[R - L + 1];
        if(L == R){
            return ;
        }
        int mid = (L + R) >> 1 ;
        sort(array , L , mid);
        sort(array , mid + 1 , R);
        merge(array , L , mid , R);
    }
    public static void merge(int[] array , int L , int mid , int R){
        int[] help = new int[R - L + 1];
        int l = 0 , r = 0 , h = 0 ;
        while(l + L <= mid && r + mid +1 <= R){
            help[h++] = (array[L + l] < array[mid + 1 + r])?array[L + l++]:array[mid + 1 + r++];
        }
        while(l + L <= mid){
            help[h++] = array[L + l++] ;
        }
        while(r + mid + 1 <= R){
            help[h++] = array[mid + 1 + r++];
        }
        for(int i = 0 ; i  < help.length ; i++){
            array[L + i] = help[i];
        }
    }

    public static void main(String[] args) {
        int[] array = { 111 , 11 , 23 , 323 , 98 ,12 ,12122 , 23423} ;
        sort(array, 0, array.length - 1);
        System.out.println(Arrays.toString(array));
    }
}