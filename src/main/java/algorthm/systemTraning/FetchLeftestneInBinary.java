package algorthm.systemTraning;


import java.util.Scanner;

/**
 * @className: FetchLeftestneInBinary
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/22 20:28
 */
public class FetchLeftestneInBinary {
    public static void main(String[] args) {
        int i = 10 ;
        System.out.println(Integer.toBinaryString(i));
        System.out.println(Integer.toBinaryString(i & (-i)));

    }
}
