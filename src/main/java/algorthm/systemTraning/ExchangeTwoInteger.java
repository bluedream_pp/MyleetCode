package algorthm.systemTraning;


/**
 * @className: ExchangeTwoInteger
 * @Description: 不使用多余的变量，交换两个数的位置。使用的是异或的性质:
 *                1) A^A = 0 ;
 *                2) 0^A = A ;
 *                3) A^B = B^A ;
 *                4) A^B^C = A^(B^C)
 *                这道题这三个性值都使用了 1) 2) 4) 这三个性质。
 * @Author: wangyifei
 * @Date: 2022/8/21 8:30
 */
public class ExchangeTwoInteger {
    public static void main(String[] args) {
        int a = 23 ;
        int b = 12 ;
        a = a ^ b ;
        b = a ^ b ;
        a = a ^ b ;
        System.out.println("a: " + a + " , b:" + b);
    }
}
