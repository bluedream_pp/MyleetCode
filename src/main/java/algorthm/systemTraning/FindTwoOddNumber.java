package algorthm.systemTraning;


/**
 * @className: FindTwoOddNumber
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/22 21:43
 */
public class FindTwoOddNumber {
    public static void main(String[] args) {
        int[] arry = { 1 , 1 , 1 , 2 , 2 , 3 , 3 , 3 , 4 , 4 , 5 , 5} ;
        find(arry);
        System.out.println("--------");
        arry = new int[]{5 , 5 , 6 , 7};
        find(arry);

        System.out.println("--------");
        arry = new int[]{5 , 5 , 6 , 7};
        find(arry);
    }
    public static void find(int[] arr){
        int AB = 0 ;
        int xor = 0 ;

        for (int i : arr) {
            AB ^= i ;
        }
        // A&(-A) ^ B&(-B)
//        for (int i : arr){
//            xor ^= i&(-i) ;
//        }
        xor = AB&(-AB);
        xor = xor&(-xor);
        int xor1 = 0 ;
        for (int i : arr) {
            if((xor & i) == 0){
                xor1 ^= i ;
            }
        }
        // A
        System.out.println("第一个数字为：" + xor1);
        System.out.println("第二个数字为: " + (xor1^AB));
    }
}
