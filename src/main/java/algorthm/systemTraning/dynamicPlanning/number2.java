public class number2{

	public static void main(String[] args){ 
		for(int i = 1 ; i < 50 ; i++){
			//if(process(i) == process2(i))
                	System.out.println(process2(i));	
		}
	}
	public static int process(int jump){
		if(1 == jump){
			return 1;
		}	
		if(2 == jump) return 2 ;
		return process(jump-1) + process(jump-2);
	}
	public static long process2(int jump){
		if(jump == 0){
			return 0 ;
		}	

	        long[] dp = new long[jump+2];
		dp[1] = 1 ;
		dp[2] = 2 ;
		for(int i = 3 ; i <= jump ; i++){
			dp[i] = dp[i-1] + dp[i-2];
		}
		return dp[jump];

	}
}
