package algorthm.systemTraning.dynamicPlanning;


/**
 * @className: RobotWalkOnTwoDim
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/3/21 13:21
 */
public class RobotWalkOnTwoDim {
    public static void main(String[] args) {
        System.out.println(walk(2, 2, 2, 2));
    }
    public static int walk(int heigth , int length , int aimx ,int aimy){
       return process(heigth , length , 0 , 0 , aimx , aimy , aimx , aimy);
    }
    public static int process(int heigth , int length , int start1 , int start2 , int rest1 , int rest2 , int aimx , int aimy){
        if(rest1 == 0 && rest2 == 0){
            return ( start1 == aimx && start2 == aimy ? 1 : 0);
        }
        if(start1 == aimx && start2 < aimy){
            return process(heigth , length , start1 , start2 + 1 , rest1 , rest2 -1 , aimx , aimy);
        }
        if(start2 == aimy && start1 < aimx){
            return process(heigth , length , start1 + 1 , start2 , rest1 - 1 , rest2 , aimx , aimy);
        }
        return process(heigth , length , start1 , start2 + 1 , rest1 , rest2 -1 , aimx , aimy)
                + process(heigth , length , start1 + 1 , start2 , rest1 - 1 , rest2 , aimx , aimy);
    }
}
