package algorthm.systemTraning;


import java.util.Arrays;

/**
 * @className: OddOccurrenceNumer
 * @Description: 异或的另外一个性质：偶数个 A 异或=0 , 由此还可以推出 奇数个 A 异或 = A。
 * @Author: wangyifei
 * @Date: 2022/8/21 8:40
 */
public class OddOccurrenceNumer {
    public static void main(String[] args) {
        int[] a = { 1,1, 3, 4 , 4 , 3 , 5 , 5 , 5};
        System.out.println(oddOccurrennceNumber(a));
    }
    public static int oddOccurrennceNumber(int[] ary){
        int tmp = 0 ;
        for (int i : ary) {
            tmp = tmp ^ i ;
        }
        return tmp ;
    }
}
