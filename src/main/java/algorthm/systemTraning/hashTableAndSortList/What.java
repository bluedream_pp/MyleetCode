package algorthm.systemTraning.hashTableAndSortList;


import javafx.collections.transformation.SortedList;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @className: What
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/15 16:03
 */
public class What {
    public static void main(String[] args) {
        /**
         * 1. hash 表的存取都是 O(1) 常数时间的。这是它最牛的地方。
         * 2. 有序表，是能够给表中的元素排序。
         * */
        TreeMap<Integer , String> map = new TreeMap<>();
        map.firstEntry();
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(10);
        Vector<String> v = new Vector<>();
        Hashtable<String,String> table = new Hashtable<>();
        // 不同的放入
        queue.offer(null);
        queue.add(null);
        try {
            queue.put(null);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            // 不同的取
            queue.peek();
            queue.poll();
            queue.remove();
            queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
