package algorthm.systemTraning.binaryTree;


/**
 * @className: Node
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/6 0:17
 */
public class Node {
    public int value ;
    public Node left ;
    public Node right;

    public Node() {}
    public Node(int val) {
        this.value = val;
    }
    public Node(int val, Node left, Node right) {
        this.value = val;
        this.left = left;
        this.right = right;
    }
}
