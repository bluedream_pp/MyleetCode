package algorthm.systemTraning.recursion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @className: GetMax
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/23 15:51
 */
public class GetMax {
    private static Logger logger = LoggerFactory.getLogger(GetMax.class);
    public static int max(int[] array , int start , int end){
        if(start == end){
            return array[end];
        }
        int mid = (end + start)/2;
        int left = max(array, start, mid);
        int right = max(array, mid + 1, end);
        if(left >= right){
            return left ;
        }else{
            return right ;
        }
    }
    public static int sum(int[] array , int start , int end){
        if(start == array.length -1){
            return array[end] ;
        }
        return array[start] + sum(array , start + 1 , end);
    }
    public static int sumMid(int[] array , int start , int end){
        if(start == end){
            return array[end];
        }
        int mid = (start + end)/2;
        int left = sumMid(array , start , mid);
        int right = sumMid(array , mid + 1 , end);
        return left + right ;
    }

    public static void main(String[] args) {
        int[] array = {32 , 23 ,1212 , 34221 , 23421};
        System.out.println(max(array, 0, array.length - 1));
        System.out.println(sum(array, 0, array.length - 1));
        System.out.println(sumMid(array, 0, array.length - 1));
    }
}
