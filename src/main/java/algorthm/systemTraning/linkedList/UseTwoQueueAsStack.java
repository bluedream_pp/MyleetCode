package algorthm.systemTraning.linkedList;


/**
 * @className: UseTwoQueueAsStack
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/12 23:18
 */
public class UseTwoQueueAsStack {
    public static void main(String[] args) {
        Stack stack = new Stack();
        for(int i = 0 ; i < 10 ; i++){
            stack.push(i);
        }
        for(int i = 0 ; i < 10 ; i++){
            System.out.println(stack.pop());
        }
    }
    public static class Stack{
        private boolean isQ1Emty = true ;
        private Queue q1 ;
        private Queue q2 ;
        private int capacity ;
        private int size ;
        private int DEFAULT_CAPACITY = 20 ;
        public Stack(){
            this.q1 = new Queue(DEFAULT_CAPACITY);
            this.q2 = new Queue(DEFAULT_CAPACITY);
            capacity = DEFAULT_CAPACITY;
        }
        public Stack(int capacity){
            this.q1 = new Queue(capacity);
            this.q2 = new Queue(capacity);
            this.capacity = capacity ;
        }
        {
            size = 0 ;
        }
        public int pop(){
            int ans = 0 ;
            if(size > 0){
               if(!isQ1Emty){
                   ans = q1.queueExit();
               }else{
                   ans = q2.queueExit();
               }
               size--;
            }else{
                throw new IndexOutOfBoundsException();
            }
            return ans ;
        }
        public void push(int i){
            if(size > capacity){
                throw new IndexOutOfBoundsException();
            }else {
                if(isQ1Emty){
                    q1.queueEnter(i);
                    while(q2.size() > 0){
                        q1.queueEnter(q2.queueExit());
                    }
                }else{
                    q2.queueEnter(i);
                    while(q1.size() > 0){
                        q2.queueEnter(q1.queueExit());
                    }
                }
                isQ1Emty = !isQ1Emty ;
                size++;
            }
        }
    }
    public static class Queue{
        private int[] tabs ;
        private int size ;
        private int capacity ;
        private int head ;
        private int tail ;
        private static final int DEFAULT_CAPACITY = 20 ;
        public Queue(){
            tabs = new int[DEFAULT_CAPACITY];
            capacity = DEFAULT_CAPACITY ;
        }
        public Queue(int capacity){
            this.tabs = new int[capacity];
            this.capacity = capacity;
        }
        {
            head = 0 ;
            tail = 0 ;
            size = 0 ;
        }
        public int size(){
            return size ;
        }
        public int queueExit(){
            int ans = -1 ;
            if(size == 0){
                throw new IndexOutOfBoundsException();
            }else{
                ans = tabs[head];
                head = (head+1)%capacity;
                size--;
            }
            return ans ;
        }
        public void queueEnter(int i){
            if(size > capacity){
                throw new IndexOutOfBoundsException();
            }else {
                tabs[tail] = i ;
                tail = (tail+1)%capacity;
                size++;
            }
        }
    }
}