package algorthm.systemTraning.linkedList;


/**
 * @className: ReverseLinkedList
 * @Description: 反转单向链表和双向链表
 * @Author: wangyifei
 * @Date: 2022/8/29 20:50
 */
public class ReverseLinkedList {
    public static SingleNode reverseSingleNode(SingleNode head){
        SingleNode pre = null ;
        SingleNode next = null ;
        while(head.getNext() != null){
            next = head.getNext();
            head.setNext(pre);
            pre = head ;
            head = next ;
        }
        head.setNext(pre);
        return head ;
    }
    public static DoubleNode reverseDoubleNodeLinkedList(DoubleNode head){
        DoubleNode pre = null ;
        DoubleNode next = null ;
        while (head.getNext() != null){
            next = head.getNext();
            head.setNext(pre);
            if(pre != null){
                pre.setPre(head);
            }
            pre = head ;
            head = next ;
        }
        head.setNext(pre);
        return head ;
    }

    public static void main(String[] args) {
//        SingleNode singleNode = NumberComparator.fakeSingleLinkedList(100, 10);
//        SingleNode singleNode1 = reverseSingleNode(singleNode);
//        while(singleNode1 != null){
//            System.out.println(singleNode1.getValue());
//            singleNode1 = singleNode1.getNext();
//        }
    }
}
