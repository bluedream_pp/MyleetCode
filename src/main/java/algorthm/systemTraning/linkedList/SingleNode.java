package algorthm.systemTraning.linkedList;


/**
 * @className: SingleNode
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/29 20:57
 */
public class SingleNode extends AbsNode{
    private SingleNode next ;

    public SingleNode getNext() {
        return next;
    }

    public void setNext(SingleNode next) {
        this.next = next;
    }
}
