package algorthm.systemTraning.linkedList;


/**
 * @className: DoubleNode
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/29 21:14
 */
public class DoubleNode extends AbsNode{
    private DoubleNode pre ;
    private DoubleNode next ;

    public DoubleNode getPre() {
        return pre;
    }

    public void setPre(DoubleNode pre) {
        this.pre = pre;
    }

    public DoubleNode getNext() {
        return next;
    }

    public void setNext(DoubleNode next) {
        this.next = next;
    }
}
