package algorthm.systemTraning.linkedList;


/**
 * @className: AbsNode
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/8/29 20:57
 */
public abstract class AbsNode {
    protected int value ;
    protected AbsNode next ;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public AbsNode getNext() {
        return next;
    }

    public void setNext(AbsNode next) {
        this.next = next;
    }
}
