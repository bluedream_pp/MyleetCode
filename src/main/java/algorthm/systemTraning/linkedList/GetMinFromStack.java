package algorthm.systemTraning.linkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @className: GetMinFromStack
 * @Description:
 * @Author: wangyifei
 * @Date: 2022/9/23 16:07
 */
public class GetMinFromStack {
    private static Logger logger = LoggerFactory.getLogger(GetMinFromStack.class);
    public static class Stack{
        private int[] tab ;
        private int size ;
        private int capacity ;
        private int head ;
        private int tail ;
        private static final int DEFUALT_CAPACITY = 20 ;
        public Stack(){
            this.size = 0 ;
            this.head = 0 ;
            this.tail = 0 ;
            this.capacity = DEFUALT_CAPACITY ;
            this.tab = new int[capacity];

        }
        public Stack(int capacity){
            this.size = 0 ;
            this.head = 0 ;
            this.tail = 0 ;
            this.capacity = capacity;
            this.tab = new int[capacity];
        }
        public void push(int i){
            if(size > capacity){
                throw new IndexOutOfBoundsException();
            }
            tab[tail++] = i ;
            size ++;
        }
        public int pop(){
            if(size <= 0){
                throw new IndexOutOfBoundsException();
            }
            int ans = tab[tail-1];
            tail--;
            size--;
            return ans ;
        }
        public int peak(){
            if(size <= 0){
                throw new IndexOutOfBoundsException();
            }
            return tab[tail-1];
        }
    }
    public static class StackWithMax{
        private Stack main = new Stack();
        private Stack max = new Stack();
        public void push(int i){
            if(main.size == 0){
                max.push(i);
            }else{
                if(i > max.peak()) {
                    max.push(i);
                }else{
                    max.push(max.peak());
                }
            }
            main.push(i);
        }
        public int pop(){
           int ans = 0 ;
           ans = main.pop();
           max.pop();
           return ans ;
        }
        public int max(){
            return max.peak();
        }
    }
    public static void main(String[] args) {
        StackWithMax s = new StackWithMax();
        for(int i = 0 ; i < 12 ; i++){
            s.push(i);
        }
        s.push(9);
        System.out.println(s.max());
    }
}
