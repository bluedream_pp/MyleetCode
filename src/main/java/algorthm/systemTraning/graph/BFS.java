package algorthm.systemTraning.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

/**
 * @className: BFS
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/5/4 17:21
 */
public class BFS {
    private static Logger logger = LoggerFactory.getLogger(BFS.class);
    public static void iterator(Graph graph){
        Deque<Node> deque = new ArrayDeque<>();
        Set<Node> uniqual = new HashSet();
        deque.addLast(graph.nodes.values().iterator().next());
        while(!deque.isEmpty()){
            Node node = deque.pollFirst();
            if(!uniqual.contains(node)){
                System.out.println(node.value);
                if(null != node.next){
                    deque.addAll(node.next);
                }
            }
        }
    }
}
