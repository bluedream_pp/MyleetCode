package algorthm.systemTraning.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @className: Main
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/5/4 17:18
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        int[][] graph = {
                {1 , 1 , 2},
                {1 , 2 , 3},
                {1 , 2 , 4},
                {1 , 2 , 5},
                {1 , 2 , 6},
                {1 , 2 , 7},
        };
        Graph g = GraphGenerator.genenrator(graph);
//        BFS.iterator(g);
//        DFS.iterate(g , (node)->{
//            System.out.println(node.value);
//        });
        DAG.traversalGraph1(g , (node)->{
            System.out.println(node.value);
            return "" ;
        });
    }
}
