package algorthm.systemTraning.graph;


import java.util.List;
import java.util.Map;

/**
 * @className: Graph
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/29 9:51
 */
public class Graph {
   public Map<Integer,Node> nodes ;
   public List<Edge> edges ;
    public Graph(){}
   public Graph(Map<Integer,Node> nodes , List<Edge> edges){
        this.nodes = nodes ;
        this.edges = edges ;
   }
}