package algorthm.systemTraning.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @className: Edge
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/29 9:51
 */
public class Edge {
    public int weight ;
    public Node from ;
    public Node to ;
}
