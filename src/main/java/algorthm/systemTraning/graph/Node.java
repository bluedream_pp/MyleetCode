package algorthm.systemTraning.graph;


import java.util.List;
import java.util.Objects;

/**
 * @className: Node
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/29 9:51
 */
public class Node {
    public int value ;
    public int in ;
    public int out ;
    public List<Node> next ;
    public List<Edge> edges ;
    public Node(int value){
        this.value = value ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return value == node.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public Node(int value , List<Node> next , List<Edge> edges){
        this.value = value ;
        this.next = next ;
        this.edges = edges ;
    }
}
