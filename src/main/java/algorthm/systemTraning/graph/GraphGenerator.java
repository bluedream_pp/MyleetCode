package algorthm.systemTraning.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @className: GraphGenerator
 * @Description:
 * @Author: wangyifei
 * @Date: 2024/4/29 10:41
 */
public class GraphGenerator {
    private static Logger logger = LoggerFactory.getLogger(GraphGenerator.class);

    /**
     * graph 是一个二维数组
     * graph[0] 中 [weight , from node value , to node value ]
     * @param graph
     * @return
     */
    public static Graph genenrator(int[][] graph){
        Graph graphRs = new Graph();
        graphRs.edges = new ArrayList<>();
        graphRs.nodes = new HashMap<>();
        for(int i = 0 ; i < graph.length ; i++){
            int weight = graph[i][0];
            int from  = graph[i][1];
            int to    = graph[i][2];
            if(!graphRs.nodes.containsKey(from)){
                graphRs.nodes.put(from , new Node(from));
            }
            if(!graphRs.nodes.containsKey(to)){
                graphRs.nodes.put(to , new Node(to));
            }
            Edge edge = new Edge();
            edge.from = graphRs.nodes.get(from);
            edge.to   = graphRs.nodes.get(to);
            edge.from.out++;
            edge.to.in++;
            edge.from.next = (null == edge.from.next ? new ArrayList() : edge.from.next);
            edge.from.next.add(edge.to);
            edge.weight = weight;
            graphRs.edges.add(edge);
        }
        return graphRs ;
    }
}
