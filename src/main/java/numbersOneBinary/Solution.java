package numbersOneBinary;

public class Solution {
    public int numberOf1(int num){
        int rs = 0 ;
        while(num != 0 ){
            if((num & 1) == 1){
                rs++;
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println(Integer.toBinaryString(num));
            num = num / 2 ;
        }
        return rs ;
    }
}
